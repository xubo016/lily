/*
SQLyog v10.2 
MySQL - 5.7.26 : Database - lily
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lily` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `lily`;

/*Table structure for table `ly_admin` */

DROP TABLE IF EXISTS `ly_admin`;

CREATE TABLE `ly_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_id` varchar(20) DEFAULT NULL COMMENT '昵称',
  `name` varchar(100) NOT NULL COMMENT '真实姓名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `tel` varchar(11) DEFAULT NULL COMMENT '电话',
  `thumb` int(11) NOT NULL DEFAULT '1' COMMENT '管理员头像',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `login_ip` varchar(100) DEFAULT NULL COMMENT '最后登录ip',
  `group_id` int(2) NOT NULL DEFAULT '1' COMMENT '管理员分组',
  `scope` varchar(20) DEFAULT '32' COMMENT '管理员权限',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `admin_cate_id` (`group_id`) USING BTREE,
  KEY `nickname` (`app_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ly_admin` */

insert  into `ly_admin`(`id`,`app_id`,`name`,`password`,`tel`,`thumb`,`create_time`,`update_time`,`login_time`,`login_ip`,`group_id`,`scope`) values (1,'admin','astp','e35b3587aaa77e782e78787137d94c8b','1',10,1510885948,1570116263,1568776341,'127.0.0.1',1,'32');

/*Table structure for table `ly_attachment` */

DROP TABLE IF EXISTS `ly_attachment`;

CREATE TABLE `ly_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` char(15) NOT NULL DEFAULT '' COMMENT '所属模块',
  `filename` char(50) NOT NULL DEFAULT '' COMMENT '文件名',
  `filepath` char(200) NOT NULL DEFAULT '' COMMENT '文件路径+文件名',
  `filesize` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `fileext` char(10) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '会员ID',
  `uploadip` char(15) NOT NULL DEFAULT '' COMMENT '上传IP',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未审核1已审核-1不通过',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL COMMENT '审核者id',
  `audit_time` int(11) NOT NULL COMMENT '审核时间',
  `use` varchar(200) DEFAULT NULL COMMENT '用处',
  `download` int(11) NOT NULL DEFAULT '0' COMMENT '下载量',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE,
  KEY `filename` (`filename`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='附件表';

/*Data for the table `ly_attachment` */

insert  into `ly_attachment`(`id`,`module`,`filename`,`filepath`,`filesize`,`fileext`,`user_id`,`uploadip`,`status`,`create_time`,`admin_id`,`audit_time`,`use`,`download`) values (10,'admin','3670a17854b0a4171b8e5741eea176fd.jpg','/uploads/admin/admin_thumb/20190904\\3670a17854b0a4171b8e5741eea176fd.jpg',53897,'jpg',1,'127.0.0.1',1,1567560356,1,1567560356,'admin_thumb',0),(15,'admin','b0e952b705bfc2477844a21d1550b03e.png','/uploads/admin/admin_thumb/20190907\\b0e952b705bfc2477844a21d1550b03e.png',107730,'png',1,'127.0.0.1',1,1567819527,1,1567819527,'admin_thumb',0),(16,'admin','83ca967ddbdd99cafaaf371fef774960.png','/uploads/admin/admin_thumb/20190907\\83ca967ddbdd99cafaaf371fef774960.png',99495,'png',1,'127.0.0.1',1,1567819579,1,1567819579,'admin_thumb',0),(17,'admin','08f6169076ec5b8722a476d63886681c.png','/uploads/admin/admin_thumb/20190907\\08f6169076ec5b8722a476d63886681c.png',22998,'png',1,'127.0.0.1',1,1567822141,1,1567822141,'admin_thumb',0),(18,'admin','bc893b59eef5c6b5d1e6483013a807ec.png','/uploads/admin/admin_thumb/20190907\\bc893b59eef5c6b5d1e6483013a807ec.png',46280,'png',1,'127.0.0.1',1,1567822151,1,1567822151,'admin_thumb',0),(19,'admin','acd7bbc6f43d7c92b1b973e480e69e41.png','/uploads/admin/admin_thumb/20190907\\acd7bbc6f43d7c92b1b973e480e69e41.png',29262,'png',1,'127.0.0.1',1,1567822206,1,1567822206,'admin_thumb',0),(20,'admin','f4327dd52c09b80f42c6360026021ea0.png','/uploads/admin/admin_thumb/20190907\\f4327dd52c09b80f42c6360026021ea0.png',62357,'png',1,'127.0.0.1',1,1567822210,1,1567822210,'admin_thumb',0),(21,'admin','1f6af26a0f2bc0c2798d13feb3c5e14a.png','/uploads/admin/admin_thumb/20190907\\1f6af26a0f2bc0c2798d13feb3c5e14a.png',48823,'png',1,'127.0.0.1',1,1567822253,1,1567822253,'admin_thumb',0),(22,'admin','41fc44ff1b3515023b2735db91366ddb.png','/uploads/admin/admin_thumb/20190907\\41fc44ff1b3515023b2735db91366ddb.png',48823,'png',1,'127.0.0.1',1,1567822256,1,1567822256,'admin_thumb',0),(29,'admin','271e4cfa01f1da22c40f81f3ace92050.png','/uploads/admin/admin_thumb/20190907\\271e4cfa01f1da22c40f81f3ace92050.png',24662,'png',1,'127.0.0.1',1,1567824343,1,1567824343,'admin_thumb',0),(30,'admin','bb832388902a7743f361672ad0b599c7.png','/uploads/admin/admin_thumb/20190907\\bb832388902a7743f361672ad0b599c7.png',23346,'png',1,'127.0.0.1',1,1567824412,1,1567824412,'admin_thumb',0),(31,'admin','ecde739d66271a11c12280bc6bb4d54e.png','/uploads/admin/admin_thumb/20190907\\ecde739d66271a11c12280bc6bb4d54e.png',21335,'png',1,'127.0.0.1',1,1567824452,1,1567824452,'admin_thumb',0),(32,'admin','570d88d44faf3497d699280cc86db6dd.png','/uploads/admin/admin_thumb/20190907\\570d88d44faf3497d699280cc86db6dd.png',30985,'png',1,'127.0.0.1',1,1567824517,1,1567824517,'admin_thumb',0),(33,'admin','ca880be40237a9915ea93786be110595.png','/uploads/admin/admin_thumb/20190907\\ca880be40237a9915ea93786be110595.png',26724,'png',1,'127.0.0.1',1,1567824552,1,1567824552,'admin_thumb',0),(34,'admin','380f5a6f25686ff73cd6936d4447935a.png','/uploads/admin/admin_thumb/20190907\\380f5a6f25686ff73cd6936d4447935a.png',39035,'png',1,'127.0.0.1',1,1567824580,1,1567824580,'admin_thumb',0),(37,'admin','c8f6516e0a961cad39d32bd21e36618c.png','/uploads/admin/admin_thumb/20190907\\c8f6516e0a961cad39d32bd21e36618c.png',36304,'png',1,'127.0.0.1',1,1567828829,1,1567828829,'admin_thumb',0),(38,'admin','f82880663e9f4f226081ebe27c888b97.png','/uploads/admin/admin_thumb/20190907\\f82880663e9f4f226081ebe27c888b97.png',26131,'png',1,'127.0.0.1',1,1567828919,1,1567828919,'admin_thumb',0),(39,'admin','186fde451e055f8236118bbc4d780037.png','/uploads/admin/admin_thumb/20190907\\186fde451e055f8236118bbc4d780037.png',37384,'png',1,'127.0.0.1',1,1567828966,1,1567828966,'admin_thumb',0),(40,'admin','d44d860c307659cd16731b4680ab2f03.png','/uploads/admin/admin_thumb/20190907\\d44d860c307659cd16731b4680ab2f03.png',36238,'png',1,'127.0.0.1',1,1567828999,1,1567828999,'admin_thumb',0),(41,'admin','44f91d0b36b9010af0c824526579ddee.png','/uploads/admin/admin_thumb/20190907\\44f91d0b36b9010af0c824526579ddee.png',20934,'png',1,'127.0.0.1',1,1567829034,1,1567829034,'admin_thumb',0),(42,'admin','9b6b915f445d63287efd4969f9192049.png','/uploads/admin/admin_thumb/20190907\\9b6b915f445d63287efd4969f9192049.png',37963,'png',1,'127.0.0.1',1,1567829083,1,1567829083,'admin_thumb',0),(43,'admin','ae46c4261ac8f3791484db7a7952188a.png','/uploads/admin/admin_thumb/20190907\\ae46c4261ac8f3791484db7a7952188a.png',34353,'png',1,'127.0.0.1',1,1567829119,1,1567829119,'admin_thumb',0),(44,'admin','1282e680819a4e9a90adb4c6be6062b6.png','/uploads/admin/admin_thumb/20190907\\1282e680819a4e9a90adb4c6be6062b6.png',28382,'png',1,'127.0.0.1',1,1567829254,1,1567829254,'admin_thumb',0),(45,'admin','048f910746f84e9a30a1470bd152fb48.png','/uploads/admin/admin_thumb/20190907\\048f910746f84e9a30a1470bd152fb48.png',36232,'png',1,'127.0.0.1',1,1567836138,1,1567836138,'admin_thumb',0),(46,'admin','9fe5be659a844216d0efef215b8a5051.png','/uploads/admin/admin_thumb/20190907\\9fe5be659a844216d0efef215b8a5051.png',26966,'png',1,'127.0.0.1',1,1567836274,1,1567836274,'admin_thumb',0),(47,'admin','c04f1d14392ea73e3df1dd5d962cc97e.png','/uploads/admin/admin_thumb/20190907\\c04f1d14392ea73e3df1dd5d962cc97e.png',32104,'png',1,'127.0.0.1',1,1567836313,1,1567836313,'admin_thumb',0),(48,'admin','30295a3da977bcb32c89fc66ba02598c.png','/uploads/admin/admin_thumb/20190907\\30295a3da977bcb32c89fc66ba02598c.png',40995,'png',1,'127.0.0.1',1,1567836366,1,1567836366,'admin_thumb',0),(49,'admin','5b26992a1659a8d176dffefa7e5ac944.png','/uploads/admin/admin_thumb/20190907\\5b26992a1659a8d176dffefa7e5ac944.png',39522,'png',1,'127.0.0.1',1,1567836401,1,1567836401,'admin_thumb',0),(50,'admin','9bb6c1156e3286665033bd4f5310ef72.png','/uploads/admin/admin_thumb/20190907\\9bb6c1156e3286665033bd4f5310ef72.png',35585,'png',1,'127.0.0.1',1,1567836427,1,1567836427,'admin_thumb',0),(51,'admin','c13fd97a634e41c912e284010c3a2be8.png','/uploads/admin/admin_thumb/20190907\\c13fd97a634e41c912e284010c3a2be8.png',25735,'png',1,'127.0.0.1',1,1567836487,1,1567836487,'admin_thumb',0),(52,'admin','8a6d36985c7f20b6aee718539ae81bb6.png','/uploads/admin/admin_thumb/20190907\\8a6d36985c7f20b6aee718539ae81bb6.png',37425,'png',1,'127.0.0.1',1,1567836527,1,1567836527,'admin_thumb',0),(53,'admin','8e7755891ea405814df3bb620cd332a1.png','/uploads/admin/admin_thumb/20190907\\8e7755891ea405814df3bb620cd332a1.png',38371,'png',1,'127.0.0.1',1,1567836554,1,1567836554,'admin_thumb',0),(54,'admin','effe9134ab793ce1c1ca131d548af525.png','/uploads/admin/admin_thumb/20190907\\effe9134ab793ce1c1ca131d548af525.png',45287,'png',1,'127.0.0.1',1,1567836585,1,1567836585,'admin_thumb',0),(55,'admin','f6da006022e224af2ce4d08b6cd0aec9.png','/uploads/admin/admin_thumb/20190907\\f6da006022e224af2ce4d08b6cd0aec9.png',32133,'png',1,'127.0.0.1',1,1567836614,1,1567836614,'admin_thumb',0),(57,'admin','ac136a7c057e6452f2f2d4f067728e54.png','/uploads/admin/admin_thumb/20190907\\ac136a7c057e6452f2f2d4f067728e54.png',33959,'png',1,'127.0.0.1',1,1567836698,1,1567836698,'admin_thumb',0),(58,'admin','e575b7e2b721e3d6357bbb05c928eaef.png','/uploads/admin/admin_thumb/20190907\\e575b7e2b721e3d6357bbb05c928eaef.png',35574,'png',1,'127.0.0.1',1,1567836724,1,1567836724,'admin_thumb',0),(59,'admin','0f675d225eb08f2b6603d66280a153fb.png','/uploads/admin/admin_thumb/20190907\\0f675d225eb08f2b6603d66280a153fb.png',28042,'png',1,'127.0.0.1',1,1567836751,1,1567836751,'admin_thumb',0),(60,'admin','dd47d3557f3462a331b468702a27ad91.png','/uploads/admin/admin_thumb/20190907\\dd47d3557f3462a331b468702a27ad91.png',35907,'png',1,'127.0.0.1',1,1567836798,1,1567836798,'admin_thumb',0),(61,'admin','31a58f6de2a1d40700fef21d15a7c828.png','/uploads/admin/admin_thumb/20190907\\31a58f6de2a1d40700fef21d15a7c828.png',38737,'png',1,'127.0.0.1',1,1567836854,1,1567836854,'admin_thumb',0),(62,'admin','6b8567a740602d834cd55375fe3e523d.png','/uploads/admin/admin_thumb/20190907\\6b8567a740602d834cd55375fe3e523d.png',32288,'png',1,'127.0.0.1',1,1567836884,1,1567836884,'admin_thumb',0),(63,'admin','26b1ff060e4e01d91b4e5bebc5eb3764.png','/uploads/admin/admin_thumb/20190907\\26b1ff060e4e01d91b4e5bebc5eb3764.png',23573,'png',1,'127.0.0.1',1,1567836915,1,1567836915,'admin_thumb',0),(64,'admin','9a624b96aea2893e2a3ca6f56b3db918.png','/uploads/admin/admin_thumb/20190907\\9a624b96aea2893e2a3ca6f56b3db918.png',36057,'png',1,'127.0.0.1',1,1567836947,1,1567836947,'admin_thumb',0),(65,'admin','126b6733cc9f740c38b2558824cf401f.png','/uploads/admin/admin_thumb/20190907\\126b6733cc9f740c38b2558824cf401f.png',23121,'png',1,'127.0.0.1',1,1567836979,1,1567836979,'admin_thumb',0),(66,'admin','8534765a2f9335bf81e4cadeb5c5b99e.png','/uploads/admin/admin_thumb/20190907\\8534765a2f9335bf81e4cadeb5c5b99e.png',31455,'png',1,'127.0.0.1',1,1567837019,1,1567837019,'admin_thumb',0),(67,'admin','70ecc4e4909c571ca948253ccaf73bd7.png','/uploads/admin/admin_thumb/20190907\\70ecc4e4909c571ca948253ccaf73bd7.png',33364,'png',1,'127.0.0.1',1,1567837472,1,1567837472,'admin_thumb',0),(68,'admin','ba41af2b9ac1baeff525c738dc6133a7.png','/uploads/admin/admin_thumb/20190907\\ba41af2b9ac1baeff525c738dc6133a7.png',26207,'png',1,'127.0.0.1',1,1567837481,1,1567837481,'admin_thumb',0),(81,'admin','4b5da7cf868568e5a4b2a48f1e61a855.png','/uploads/admin/admin_thumb/20190908\\4b5da7cf868568e5a4b2a48f1e61a855.png',29430,'png',1,'127.0.0.1',1,1567912857,1,1567912857,'admin_thumb',0),(98,'admin','2ba368ce65a18821af206d5ed43fae44.png','/uploads/admin/admin_thumb/20190912\\2ba368ce65a18821af206d5ed43fae44.png',70484,'png',1,'127.0.0.1',1,1568258177,1,1568258177,'admin_thumb',0),(99,'admin','a4c3cf39c43bea2a9ff5ea34be35417a.png','/uploads/admin/admin_thumb/20190912\\a4c3cf39c43bea2a9ff5ea34be35417a.png',13892,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(100,'admin','11b58c380994a7c3cf81e0e1d7773052.png','/uploads/admin/admin_thumb/20190912\\11b58c380994a7c3cf81e0e1d7773052.png',51710,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(101,'admin','fa6792dcf326c51a0164b9193899132e.png','/uploads/admin/admin_thumb/20190912\\fa6792dcf326c51a0164b9193899132e.png',40603,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(102,'admin','4e31b692efcfcbc07da07c7e75bf0bae.png','/uploads/admin/admin_thumb/20190912\\4e31b692efcfcbc07da07c7e75bf0bae.png',78077,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(103,'admin','37188b4146ae7994ed5d80aaa392608d.png','/uploads/admin/admin_thumb/20190912\\37188b4146ae7994ed5d80aaa392608d.png',108290,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(104,'admin','ee43d00e82fa6b1bd7761264b7bf0b2c.png','/uploads/admin/admin_thumb/20190912\\ee43d00e82fa6b1bd7761264b7bf0b2c.png',28937,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(105,'admin','4187cf045ac403e45302efd4d6c5ee1b.png','/uploads/admin/admin_thumb/20190912\\4187cf045ac403e45302efd4d6c5ee1b.png',95001,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(106,'admin','5c36366b45cffb190f5304870966b696.png','/uploads/admin/admin_thumb/20190912\\5c36366b45cffb190f5304870966b696.png',16891,'png',1,'127.0.0.1',1,1568258178,1,1568258178,'admin_thumb',0),(107,'admin','46e4a82986ccd0ef839b459973182e5a.png','/uploads/admin/admin_thumb/20190912\\46e4a82986ccd0ef839b459973182e5a.png',48063,'png',1,'127.0.0.1',1,1568258179,1,1568258179,'admin_thumb',0),(108,'admin','049e4299d26a8b063606c68cb64c3ad2.png','/uploads/admin/admin_thumb/20190912\\049e4299d26a8b063606c68cb64c3ad2.png',67676,'png',1,'127.0.0.1',1,1568258179,1,1568258179,'admin_thumb',0),(109,'admin','cd8ddad9f07bcfa31f936a44486fd5c5.png','/uploads/admin/admin_thumb/20190912\\cd8ddad9f07bcfa31f936a44486fd5c5.png',48100,'png',1,'127.0.0.1',1,1568258179,1,1568258179,'admin_thumb',0),(110,'admin','dc250676d40d135a4c5d99a54d3d1c68.png','/uploads/admin/admin_thumb/20190912\\dc250676d40d135a4c5d99a54d3d1c68.png',78025,'png',1,'127.0.0.1',1,1568258179,1,1568258179,'admin_thumb',0),(111,'admin','570d5df488cd24430c4e4fb3a0dd9499.png','/uploads/admin/admin_thumb/20190912\\570d5df488cd24430c4e4fb3a0dd9499.png',33034,'png',1,'127.0.0.1',1,1568273513,1,1568273513,'admin_thumb',0),(112,'admin','27697712759f0635d79df9b8fa51aea7.png','/uploads/admin/admin_thumb/20190912\\27697712759f0635d79df9b8fa51aea7.png',55193,'png',1,'127.0.0.1',1,1568273531,1,1568273531,'admin_thumb',0);

/*Table structure for table `ly_auth_group` */

DROP TABLE IF EXISTS `ly_auth_group`;

CREATE TABLE `ly_auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `permissions` text COMMENT '权限菜单',
  `create_time` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1默认开启2默认关闭',
  `desc` text COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `name` (`name`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `ly_auth_group` */

insert  into `ly_auth_group`(`id`,`name`,`permissions`,`create_time`,`status`,`desc`) values (1,'超级管理员','2,1,11,12,3,8,9,10,4,5,6,7,17,18,13,14,20,19,21,32,22,23,24,25,26,27,28,29,30,31,33,40,34,35,36,37,38,39,50,41,42,43,44,45,46,47,48,49,51,52,53',0,1,'超级管理员，拥有最高权限！');

/*Table structure for table `ly_auth_rule` */

DROP TABLE IF EXISTS `ly_auth_rule`;

CREATE TABLE `ly_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(20) NOT NULL DEFAULT '' COMMENT '名称',
  `module` char(50) DEFAULT NULL COMMENT '模块',
  `controller` char(100) DEFAULT NULL COMMENT '控制器',
  `function` char(100) DEFAULT NULL COMMENT '方法',
  `parameter` char(50) DEFAULT NULL COMMENT '参数',
  `desc` varchar(250) DEFAULT NULL COMMENT '描述',
  `is_display` int(1) NOT NULL DEFAULT '1' COMMENT '1显示在左侧菜单2只作为操作节点',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1权限节点2普通节点',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态1为启用',
  `condition` char(100) DEFAULT '',
  `pid` int(5) NOT NULL DEFAULT '0' COMMENT '父栏目0为顶级菜单',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `is_open` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0默认闭合1默认展开',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='权限节点';

/*Data for the table `ly_auth_rule` */

insert  into `ly_auth_rule`(`id`,`name`,`module`,`controller`,`function`,`parameter`,`desc`,`is_display`,`type`,`icon`,`status`,`condition`,`pid`,`create_time`,`sort`,`is_open`) values (1,'管理员','admin','admin','lst','','管理员列表                                                  ',1,1,'fa-user',1,'',2,NULL,0,0),(32,'短信设置','admin','SmsConfig','index','','短信配置首页。           ',1,1,'fa-comments',1,'',21,1554731788,0,0),(2,'系统菜单','','','','','系统菜单              ',1,1,'fa-cog',1,'',0,NULL,2,0),(3,'权限组','admin','auth_group','lst','','用户组列表          ',1,1,'fa-dot-circle-o',1,'',2,NULL,0,0),(4,'系统菜单','admin','auth_rule','lst','','系统列表                    ',1,1,'fa-share-alt',1,'',2,NULL,0,0),(5,'删除系统菜单','admin','AuthRule','delete','','删除系统菜单',2,1,'',1,'',4,NULL,0,0),(6,'添加系统菜单','admin','AuthRule','addEdit','','添加修改系统菜单',2,1,'',1,'',4,NULL,0,0),(7,'系统菜单排序','admin','base','sort','','系统菜单排序',2,1,'',1,'',4,NULL,0,0),(8,'添加权限组','admin','AuthGroup','addEdit','','权限组新增/修改            ',2,1,'',1,'',3,1552967170,0,0),(9,'删除权限组','admin','AuthGroup','delete','','删除权限组',2,1,'',1,'',3,1552967269,0,0),(10,'权限组预览','admin','AuthGroup','preview','','权限组预览',2,1,'',1,'',3,1552967327,0,0),(11,'新增管理员','admin','admin','addEdit','','新增修改管理员                  ',2,1,'',1,'',1,1553044733,0,0),(12,'删除管理员','admin','admin','delete','','删除管理员',2,1,'',1,'',1,1553044826,0,0),(13,'个人','','','','','个人信息修改                           ',1,1,'fa-user',1,'',0,1553045051,1,0),(14,'个人信息','admin','admin','personal','','个人信息修改    ',1,1,'fa-user',1,'',13,1553045803,0,0),(15,'管理员登录','admin','login','index','','管理员登录',2,2,'',1,'',0,1553091902,1,0),(16,'管理员退出','admin','login','logout','','管理员退出                       ',2,2,'',1,'',0,1553091936,1,0),(17,'网站设置','admin','webConfig','index','','网站设置   ',1,1,'fa-bullseye',1,'',2,1553093381,0,0),(18,'修改网站设置','admin','webConfig','edit','','修改网站设置                  ',2,1,'',1,'',17,1553093739,0,0),(19,'图片上传','admin','common','upload','','图片上传                       ',2,1,'',1,'',0,1553183549,1,0),(20,'修改密码','admin','admin','editpassword','','管理员修改个人密码。',1,1,'fa-unlock-alt',1,'',13,1553184676,0,0),(21,'内容','','','','','内容管理   ',1,1,'fa-th-large',1,'',0,1553256973,1,0),(22,'附件','admin','attachment','lst','','附件列表           ',1,1,'fa-cube',1,'',21,1553257092,0,0),(23,' 附件审核','admin','attachment','audit','','附件审核。         ',2,1,'',1,'',22,1553257199,0,0),(24,'附件上传','admin','attachment','upload','','附件上传。',2,1,'',1,'',22,1553257246,0,0),(25,'附件下载','admin','attachment','download','','附件下载       ',2,1,'',1,'',22,1553257295,0,0),(26,'附件删除','admin','attachment','delete','','附件删除',2,1,'',1,'',22,1553257334,0,0),(27,'操作日志','admin','log','index','','管理员操作日志     ',1,1,'fa-pencil',1,'',21,1553273460,0,0),(28,'URL设置','admin','urlconfig','lst','','URL设置列表             ',1,1,'fa-code-fork',1,'',21,1553341204,0,0),(29,'新增/修改url设置','admin','urlconfig','addEdit','','新增/修改url设置。              ',2,1,'',1,'',28,1553341264,0,0),(30,'启用/禁用url美化','admin','urlconfig','status','','启用/禁用url美化                      ',2,1,'',1,'',28,1553341305,0,0),(31,'删除url美化规则','admin','urlconfig','delete','','删除url美化规则。          ',2,1,'',1,'',28,1553341354,0,0),(33,'商城管理','','','','','商城管理',1,1,'fa-shopping-bag',1,'',0,NULL,0,0),(34,'轮播图','admin','banner','lst','','轮播图',1,1,'',1,'',40,NULL,0,0),(35,'新增轮播图','admin','banner','addEdit','','新增/修改轮播图',2,1,'',1,'',34,NULL,0,0),(36,'删除轮播图','admin','banner','del','','删除轮播图',2,1,'',1,'',34,NULL,0,0),(37,'轮播项','admin','bannerItem','lst','','轮播项',1,1,'',1,'',40,1563874278,0,0),(38,'新增轮播','admin','bannerItem','addEdit','','新增/修改轮播',2,1,'',1,'',37,1563874371,0,0),(39,'删除轮播项','admin','bannerItem','del','','删除轮播项',2,1,'',1,'',37,1563874424,0,0),(40,'轮播','','','','','轮播管理',1,1,'',1,'',33,1563874593,0,0),(41,'主题','admin','theme','lst','','精选主题',1,1,'',1,'',50,1567819743,0,0),(42,'新增主题','admin','theme','addEdit','','新增修改主题',2,1,'',1,'',41,1567820412,0,0),(43,'删除主题','admin','theme','del','','删除主题',2,1,'',1,'',41,1567820463,0,0),(44,'分类','admin','category','lst','','分类',1,1,'',1,'',50,1567823452,0,0),(45,'新增分类','admin','category','addEdit','','新增修改分类',2,1,'',1,'',44,1567823489,0,0),(46,'删除分类','admin','category','del','','删除分类',2,1,'',1,'',44,1567823518,0,0),(47,'产品','admin','product','lst','','产品列表',1,1,'',1,'',50,1567824917,0,0),(48,'新增产品','admin','product','addEdit','','新增修改产品',2,1,'',1,'',47,1567824952,0,0),(49,'删除产品','admin','product','del','','删除产品',2,1,'',1,'',47,1567824982,0,0),(50,'产品','','','','','产品管理',1,1,'',1,'',33,1567992162,0,0),(51,'用户','admin','user','lst','','用户列表',1,1,'',1,'',33,1567992278,0,0),(52,'删除用户','admin','user','del','','删除用户',2,1,'',1,'',51,1567992327,0,0),(53,'订单','admin','order','lst','','订单管理',1,1,'',1,'',33,1568594126,0,0);

/*Table structure for table `ly_banner` */

DROP TABLE IF EXISTS `ly_banner`;

CREATE TABLE `ly_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '轮播图表',
  `name` varchar(50) DEFAULT NULL COMMENT 'banner名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `delete_time` int(11) DEFAULT NULL COMMENT '软删除',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `ly_banner` */

insert  into `ly_banner`(`id`,`name`,`description`,`delete_time`,`update_time`) values (1,'首页','首页顶部轮播图',NULL,1563873632);

/*Table structure for table `ly_banner_item` */

DROP TABLE IF EXISTS `ly_banner_item`;

CREATE TABLE `ly_banner_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '轮播项表',
  `img_id` int(11) DEFAULT NULL COMMENT '外键 关联img表',
  `banner_id` int(11) DEFAULT NULL COMMENT '外键关联banner表',
  `key_word` varchar(100) DEFAULT NULL COMMENT '执行关键字，根据不同type含义不同',
  `type` tinyint(4) DEFAULT NULL COMMENT '1、导向商品 2、可能导向分类',
  `delete_time` int(11) DEFAULT NULL COMMENT '软删除',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `ly_banner_item` */

insert  into `ly_banner_item`(`id`,`img_id`,`banner_id`,`key_word`,`type`,`delete_time`,`update_time`) values (1,15,1,'shop',1,NULL,1567819529),(4,16,1,'shop',1,NULL,1567819586),(6,111,1,'shop',1,NULL,1568273522),(7,112,1,'shop',1,NULL,1568273540);

/*Table structure for table `ly_category` */

DROP TABLE IF EXISTS `ly_category`;

CREATE TABLE `ly_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `topic_img_id` int(11) DEFAULT NULL COMMENT '外键，关联image表',
  `delete_time` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COMMENT='商品类目';

/*Data for the table `ly_category` */

insert  into `ly_category`(`id`,`name`,`topic_img_id`,`delete_time`,`description`,`update_time`) values (2,'果味',29,NULL,'',1567824344),(3,'蔬菜',30,NULL,'',1567824415),(4,'炒货',31,NULL,'',1567824453),(5,'点心',32,NULL,'',1567824518),(6,'粗茶',33,NULL,'',1567824552),(7,'淡饭',34,NULL,'',1567824581);

/*Table structure for table `ly_log` */

DROP TABLE IF EXISTS `ly_log`;

CREATE TABLE `ly_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NOT NULL COMMENT '操作菜单id',
  `admin_id` int(11) DEFAULT NULL COMMENT '操作者id',
  `ip` varchar(100) DEFAULT NULL COMMENT '操作ip',
  `operation_id` varchar(200) DEFAULT NULL COMMENT '操作关联id',
  `create_time` int(11) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `admin_id` (`admin_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `ly_log` */

insert  into `ly_log`(`id`,`rule_id`,`admin_id`,`ip`,`operation_id`,`create_time`) values (1,6,1,'127.0.0.1','1',1553436083),(2,14,1,'127.0.0.1','1',1553442033),(3,18,1,'127.0.0.1','',1570117046);

/*Table structure for table `ly_order` */

DROP TABLE IF EXISTS `ly_order`;

CREATE TABLE `ly_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(20) NOT NULL COMMENT '订单号',
  `user_id` int(11) NOT NULL COMMENT '外键，用户id，注意并不是openid',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `total_price` decimal(6,2) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1:未支付， 2：已支付，3：已发货 , 4: 已支付，但库存不足',
  `snap_img` varchar(255) DEFAULT NULL COMMENT '订单快照图片',
  `snap_name` varchar(80) DEFAULT NULL COMMENT '订单快照名称',
  `total_count` int(11) NOT NULL DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `snap_items` text COMMENT '订单其他信息快照（json)',
  `snap_address` varchar(500) DEFAULT NULL COMMENT '地址快照',
  `prepay_id` varchar(100) DEFAULT NULL COMMENT '订单微信支付的预订单id（用于发送模板消息）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_no` (`order_no`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `ly_order` */

/*Table structure for table `ly_order_product` */

DROP TABLE IF EXISTS `ly_order_product`;

CREATE TABLE `ly_order_product` (
  `order_id` int(11) NOT NULL COMMENT '联合主键，订单id',
  `product_id` int(11) NOT NULL COMMENT '联合主键，商品id',
  `count` int(11) NOT NULL COMMENT '商品数量',
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `ly_order_product` */

insert  into `ly_order_product`(`order_id`,`product_id`,`count`,`delete_time`,`update_time`) values (1,1,3,NULL,1568796960),(2,1,3,NULL,1568797051),(3,1,3,NULL,1568797086),(4,1,3,NULL,1568797110),(5,1,3,NULL,1568797222),(6,1,3,NULL,1568797438),(7,1,3,NULL,1568797464),(8,1,3,NULL,1568797481),(1,2,1,NULL,1568796960),(2,2,1,NULL,1568797051),(3,2,1,NULL,1568797086),(4,2,1,NULL,1568797110),(5,2,1,NULL,1568797222),(6,2,1,NULL,1568797438),(7,2,1,NULL,1568797464),(8,2,1,NULL,1568797481);

/*Table structure for table `ly_product` */

DROP TABLE IF EXISTS `ly_product`;

CREATE TABLE `ly_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL COMMENT '商品名称',
  `price` decimal(6,2) NOT NULL COMMENT '价格,单位：分',
  `stock` int(11) NOT NULL DEFAULT '0' COMMENT '库存量',
  `delete_time` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `main_img_url` varchar(255) DEFAULT NULL COMMENT '主图ID号，这是一个反范式设计，有一定的冗余',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL,
  `summary` varchar(50) DEFAULT NULL COMMENT '摘要',
  `img_id` int(11) DEFAULT NULL COMMENT '图片外键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ly_product` */

insert  into `ly_product`(`id`,`name`,`price`,`stock`,`delete_time`,`category_id`,`main_img_url`,`create_time`,`update_time`,`summary`,`img_id`) values (1,'芹菜 半斤','0.01',999,NULL,3,'/uploads/admin/admin_thumb/20190907\\8534765a2f9335bf81e4cadeb5c5b99e.png',NULL,1568597073,'',66),(2,'梨花带雨 3个','0.01',984,NULL,2,'/uploads/admin/admin_thumb/20190907\\126b6733cc9f740c38b2558824cf401f.png',NULL,1567836980,'',65),(3,'素米 327克','0.01',996,NULL,7,'/uploads/admin/admin_thumb/20190907\\9a624b96aea2893e2a3ca6f56b3db918.png',NULL,1567836948,'',64),(4,'红袖枸杞 6克*3袋','0.01',998,NULL,6,'/uploads/admin/admin_thumb/20190907\\26b1ff060e4e01d91b4e5bebc5eb3764.png',NULL,1567836916,'',63),(5,'春生龙眼 500克','0.01',995,NULL,2,'/uploads/admin/admin_thumb/20190907\\6b8567a740602d834cd55375fe3e523d.png',NULL,1567836884,'',62),(6,'小红的猪耳朵 120克','0.01',997,NULL,5,'/uploads/admin/admin_thumb/20190907\\31a58f6de2a1d40700fef21d15a7c828.png',NULL,1567836855,'',61),(7,'泥蒿 半斤','0.01',998,NULL,3,'/uploads/admin/admin_thumb/20190907\\dd47d3557f3462a331b468702a27ad91.png',NULL,1567836799,'',60),(8,'夏日芒果 3个','0.01',995,NULL,2,'/uploads/admin/admin_thumb/20190907\\0f675d225eb08f2b6603d66280a153fb.png',NULL,1567836752,'',59),(9,'冬木红枣 500克','0.01',996,NULL,2,'/uploads/admin/admin_thumb/20190907\\e575b7e2b721e3d6357bbb05c928eaef.png',NULL,1567836725,'',58),(10,'万紫千凤梨 300克','0.01',996,NULL,2,'/uploads/admin/admin_thumb/20190907\\ac136a7c057e6452f2f2d4f067728e54.png',NULL,1567836699,'',57),(11,'贵妃笑 100克','0.01',994,NULL,2,'/uploads/admin/admin_thumb/20190908\\4b5da7cf868568e5a4b2a48f1e61a855.png',NULL,1568258406,'',81),(12,'珍奇异果 3个','0.01',999,NULL,2,'/uploads/admin/admin_thumb/20190907\\f6da006022e224af2ce4d08b6cd0aec9.png',NULL,1567836615,'',55),(13,'绿豆 125克','0.01',999,NULL,7,'/uploads/admin/admin_thumb/20190907\\effe9134ab793ce1c1ca131d548af525.png',NULL,1567836586,'',54),(14,'芝麻 50克','0.01',999,NULL,7,'/uploads/admin/admin_thumb/20190907\\8e7755891ea405814df3bb620cd332a1.png',NULL,1567836555,'',53),(15,'猴头菇 370克','0.01',999,NULL,7,'/uploads/admin/admin_thumb/20190907\\8a6d36985c7f20b6aee718539ae81bb6.png',NULL,1567836528,'',52),(16,'西红柿 1斤','0.01',999,NULL,3,'/uploads/admin/admin_thumb/20190907\\c13fd97a634e41c912e284010c3a2be8.png',NULL,1567836488,'',51),(17,'油炸花生 300克','0.01',999,NULL,4,'/uploads/admin/admin_thumb/20190907\\9bb6c1156e3286665033bd4f5310ef72.png',NULL,1567836427,'',50),(18,'春泥西瓜子 128克','0.01',997,NULL,4,'/uploads/admin/admin_thumb/20190907\\5b26992a1659a8d176dffefa7e5ac944.png',NULL,1567836403,'',49),(19,'碧水葵花籽 128克','0.01',999,NULL,4,'/uploads/admin/admin_thumb/20190907\\30295a3da977bcb32c89fc66ba02598c.png',NULL,1567836367,'',48),(20,'碧螺春 12克*3袋','0.01',999,NULL,6,'/uploads/admin/admin_thumb/20190907\\c04f1d14392ea73e3df1dd5d962cc97e.png',NULL,1567836314,'',47),(21,'西湖龙井 8克*3袋','0.01',998,NULL,6,'/uploads/admin/admin_thumb/20190907\\9fe5be659a844216d0efef215b8a5051.png',NULL,1567836275,'',46),(22,'梅兰清花糕 1个','0.01',997,NULL,5,'/uploads/admin/admin_thumb/20190907\\ba41af2b9ac1baeff525c738dc6133a7.png',NULL,1567837482,'',68),(23,'清凉薄荷糕 1个','0.01',998,NULL,5,'/uploads/admin/admin_thumb/20190907\\70ecc4e4909c571ca948253ccaf73bd7.png',NULL,1567837473,'',67),(25,'小明的妙脆角 120克','0.01',999,NULL,5,'/uploads/admin/admin_thumb/20190907\\048f910746f84e9a30a1470bd152fb48.png',NULL,1567836139,'',45),(26,'红衣青瓜 混搭160克','0.01',999,NULL,2,'/uploads/admin/admin_thumb/20190907\\1282e680819a4e9a90adb4c6be6062b6.png',NULL,1567829257,'',44),(27,'锈色瓜子 100克','0.01',998,NULL,4,'/uploads/admin/admin_thumb/20190907\\ae46c4261ac8f3791484db7a7952188a.png',NULL,1567829120,'',43),(28,'春泥花生 200克','0.01',999,NULL,4,'/uploads/admin/admin_thumb/20190907\\9b6b915f445d63287efd4969f9192049.png',NULL,1567829083,'',42),(29,'冰心鸡蛋 2个','0.01',999,NULL,7,'/uploads/admin/admin_thumb/20190907\\44f91d0b36b9010af0c824526579ddee.png',NULL,1567829035,'',41),(30,'八宝莲子 200克','0.01',999,NULL,7,'/uploads/admin/admin_thumb/20190907\\d44d860c307659cd16731b4680ab2f03.png',NULL,1567829002,'',40),(31,'深涧木耳 78克','0.01',999,NULL,7,'/uploads/admin/admin_thumb/20190907\\186fde451e055f8236118bbc4d780037.png',NULL,1567828969,'',39),(32,'土豆 半斤','0.01',999,NULL,3,'/uploads/admin/admin_thumb/20190907\\f82880663e9f4f226081ebe27c888b97.png',NULL,1567828921,'',38),(33,'青椒 半斤','0.01',999,NULL,3,'/uploads/admin/admin_thumb/20190907\\c8f6516e0a961cad39d32bd21e36618c.png',NULL,1570244146,'',37);

/*Table structure for table `ly_product_image` */

DROP TABLE IF EXISTS `ly_product_image`;

CREATE TABLE `ly_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_id` int(11) NOT NULL COMMENT '外键，关联图片表',
  `delete_time` int(11) DEFAULT NULL COMMENT '状态，主要表示是否删除，也可以扩展其他状态',
  `product_id` int(11) NOT NULL COMMENT '商品id，外键',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ly_product_image` */

insert  into `ly_product_image`(`id`,`img_id`,`delete_time`,`product_id`) values (67,106,NULL,11),(68,109,NULL,11),(69,107,NULL,11),(70,110,NULL,11),(71,108,NULL,11),(72,98,NULL,11),(73,99,NULL,11),(74,100,NULL,11),(75,101,NULL,11),(76,102,NULL,11),(77,103,NULL,11),(78,104,NULL,11),(79,105,NULL,11);

/*Table structure for table `ly_product_property` */

DROP TABLE IF EXISTS `ly_product_property`;

CREATE TABLE `ly_product_property` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT '' COMMENT '详情属性名称',
  `detail` varchar(255) NOT NULL COMMENT '详情属性',
  `product_id` int(11) NOT NULL COMMENT '商品id，外键',
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ly_product_property` */

insert  into `ly_product_property`(`id`,`name`,`detail`,`product_id`,`delete_time`,`update_time`) values (1,'品名','杨梅',11,NULL,NULL),(2,'口味','青梅味 雪梨味 黄桃味 菠萝味',11,NULL,NULL),(3,'产地','火星',11,NULL,NULL),(4,'保质期','180天',11,NULL,NULL),(5,'品名','梨子',2,NULL,NULL),(6,'产地','金星',2,NULL,NULL),(7,'净含量','100g',2,NULL,NULL),(8,'保质期','10天',2,NULL,NULL),(17,'品名','青椒',33,NULL,NULL),(18,'产地','地球',33,NULL,NULL);

/*Table structure for table `ly_smsconfig` */

DROP TABLE IF EXISTS `ly_smsconfig`;

CREATE TABLE `ly_smsconfig` (
  `sms` varchar(10) NOT NULL DEFAULT 'sms' COMMENT '标识',
  `appkey` varchar(200) NOT NULL,
  `secretkey` varchar(200) NOT NULL,
  `type` varchar(100) DEFAULT 'normal' COMMENT '短信类型',
  `name` varchar(100) NOT NULL COMMENT '短信签名',
  `code` varchar(100) NOT NULL COMMENT '短信模板ID',
  `content` text NOT NULL COMMENT '短信默认模板',
  KEY `sms` (`sms`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `ly_smsconfig` */

insert  into `ly_smsconfig`(`sms`,`appkey`,`secretkey`,`type`,`name`,`code`,`content`) values ('sms','12','12','normal','赤峰天一建材装饰店','SMS_172430066','1234');

/*Table structure for table `ly_theme` */

DROP TABLE IF EXISTS `ly_theme`;

CREATE TABLE `ly_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL COMMENT '专题名称',
  `description` varchar(255) DEFAULT NULL COMMENT '专题描述',
  `topic_img_id` int(11) NOT NULL COMMENT '主题图，外键',
  `head_img_id` int(11) NOT NULL COMMENT '专题列表页，头图',
  `delete_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COMMENT='主题信息表';

/*Data for the table `ly_theme` */

insert  into `ly_theme`(`id`,`name`,`description`,`topic_img_id`,`head_img_id`,`delete_time`,`update_time`) values (1,'美味水果世界','美味水果世界',17,18,NULL,1567840653),(2,'新品推荐','新品推荐',19,20,NULL,1567840661),(3,'做个干物女','做个干物女',21,22,NULL,1567840669);

/*Table structure for table `ly_theme_product` */

DROP TABLE IF EXISTS `ly_theme_product`;

CREATE TABLE `ly_theme_product` (
  `theme_id` int(11) NOT NULL COMMENT '主题外键',
  `product_id` int(11) NOT NULL COMMENT '商品外键',
  PRIMARY KEY (`theme_id`,`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主题所包含的商品';

/*Data for the table `ly_theme_product` */

insert  into `ly_theme_product`(`theme_id`,`product_id`) values (1,2),(1,5),(1,8),(1,10),(1,12),(2,1),(2,2),(2,3),(2,5),(2,6),(2,16),(2,33),(3,15),(3,18),(3,19),(3,27),(3,30),(3,31);

/*Table structure for table `ly_urlconfig` */

DROP TABLE IF EXISTS `ly_urlconfig`;

CREATE TABLE `ly_urlconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aliases` varchar(200) NOT NULL COMMENT '想要设置的别名',
  `url` varchar(200) NOT NULL COMMENT '原url结构',
  `desc` text COMMENT '备注',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '0禁用1使用',
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`) USING BTREE,
  KEY `status` (`status`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `ly_urlconfig` */

insert  into `ly_urlconfig`(`id`,`aliases`,`url`,`desc`,`status`,`create_time`) values (1,'login','base/login/index','后台登录地址。',0,1517621629);

/*Table structure for table `ly_user` */

DROP TABLE IF EXISTS `ly_user`;

CREATE TABLE `ly_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) NOT NULL,
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `headimgurl` varchar(200) DEFAULT NULL COMMENT '微信头像',
  `extend` varchar(255) DEFAULT NULL COMMENT '扩展',
  `delete_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL COMMENT '注册时间',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `openid` (`openid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ly_user` */

insert  into `ly_user`(`id`,`openid`,`nickname`,`headimgurl`,`extend`,`delete_time`,`create_time`,`update_time`) values (7,'o4xoK46HD-Rmhh7WbIJsvb0npqX0',NULL,NULL,NULL,NULL,1568040723,1568040723);

/*Table structure for table `ly_user_address` */

DROP TABLE IF EXISTS `ly_user_address`;

CREATE TABLE `ly_user_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL COMMENT '收获人姓名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `province` varchar(20) DEFAULT NULL COMMENT '省',
  `city` varchar(20) DEFAULT NULL COMMENT '市',
  `country` varchar(20) DEFAULT NULL COMMENT '区',
  `detail` varchar(100) DEFAULT NULL COMMENT '详细地址',
  `delete_time` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL COMMENT '外键',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `ly_user_address` */

insert  into `ly_user_address`(`id`,`name`,`mobile`,`province`,`city`,`country`,`detail`,`delete_time`,`user_id`,`update_time`) values (1,'xubo','18888888888','艾泽拉斯','暴风城','黑水之塔','狮王之傲旅店',NULL,7,1568376801);

/*Table structure for table `ly_web_config` */

DROP TABLE IF EXISTS `ly_web_config`;

CREATE TABLE `ly_web_config` (
  `web` varchar(20) NOT NULL COMMENT '网站配置标识',
  `name` varchar(200) NOT NULL COMMENT '网站名称',
  `keywords` text COMMENT '关键词',
  `desc` text COMMENT '描述',
  `is_log` int(1) NOT NULL DEFAULT '1' COMMENT '1开启日志0关闭',
  `file_type` varchar(200) DEFAULT NULL COMMENT '允许上传的类型',
  `file_size` bigint(20) DEFAULT NULL COMMENT '允许上传的最大值',
  `statistics` text COMMENT '统计代码',
  `black_ip` text COMMENT 'ip黑名单',
  `url_suffix` varchar(20) DEFAULT NULL COMMENT 'url伪静态后缀',
  KEY `web` (`web`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `ly_web_config` */

insert  into `ly_web_config`(`web`,`name`,`keywords`,`desc`,`is_log`,`file_type`,`file_size`,`statistics`,`black_ip`,`url_suffix`) values ('web','后台管理框架','后台管理,thinkphp5,layui','基于ThinkPHP5.1.38 + layui2.2.45 + ECharts + Mysql开发的后台管理框架，集成了一般应用所必须的基础性功能，为开发者节省大量的时间。',0,'jpg,png,gif,mp4,zip,jpeg',1000,'','',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
