<?php
namespace app\lib\enum;

class ScopeEnum
{
  // scope=16 代表App用户权限数值
  const User = 16;

  // scope=32 代表CMS管理员权限数值
  const Super = 32;
  
}
?>