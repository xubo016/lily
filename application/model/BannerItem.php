<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 轮播图
// +----------------------------------------------------------------------
namespace app\model;

class BannerItem extends BaseModel
{
  protected $hidden = ['id', 'img_id', 'banner_id', 'delete_time','update_time'];
  
  public function banner()
  {
    //关联轮播图表 一对一
    return $this->belongsTo('banner','banner_id','id');
  }

  public function img()
  {
    // 关联附件表 一对一
    return $this->belongsTo('attachment','img_id','id');
  }
}
?>