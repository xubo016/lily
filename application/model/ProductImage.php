<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 商品图片
// +----------------------------------------------------------------------
namespace app\model;

class ProductImage extends BaseModel
{
  protected $hidden = ['delete_time','img_id','product_id'];

  public function imgUrl()
  {
    // 关联附件表 表关系一对一
    return $this->belongsTo('attachment','img_id','id');
  }

}
?>