<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 基类
// +----------------------------------------------------------------------
namespace app\model;

use think\Model;

class BaseModel extends Model
{
  protected $hidden = ['delete_time'];
  
  // 图片路径组合
  public function prefixImgUrl($value,$data){
    return config('setting.img_prefix').$value;
  }

}
?>