<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 产品
// +----------------------------------------------------------------------
namespace app\model;

class Product extends BaseModel
{
  protected $hidden = ['delete_time','update_time','create_time','pivot','category_id'];

  public function categorys()
  {
    //关联产品分类表 关系一对一
    return $this->belongsTo('category','category_id','id');
  }

  public function themes()
  {
    // 关联产品表 表关系多对多
    return $this->belongsToMany('theme','theme_product','theme_id','product_id');
  }

  public function properties()
  {
    // 关联商品详情 表关系一对一
    return $this->hasMany('productProperty','product_id','id');
  }

  public function imgs()
  {
    // 关联商品图片信息 表关系一对多
    return $this->hasMany('productImage','product_id','id');
  }

  // 图片获取器
  public function getMainImgUrlAttr($value,$data) 
  {
    $value = str_replace('\\', '/', $value);
    return $this->prefixImgUrl($value,$data);
  }

  // 最近新品
  public static function getMostRecent($count)
  {
    $product = self::limit($count)->order('update_time desc')->select();
    return $product;
  }

  // 分类商品信息
  public static function getProductsByCategoryID($categoryID)
  {
    $products = self::where('category_id','=',$categoryID)->select();
    return $products;
  }

  // 单条商品
  public static function getProductDetail($id)
  {
    $product = self::with([
        'imgs' => function($query){
          $query->with(['imgUrl'])
            ->order('id asc');
        },'properties'
      ])
      ->find($id);
    return $product;
  }
}
?>