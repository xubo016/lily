<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 日志模型
// +----------------------------------------------------------------------
namespace app\model;

use think\Model;

class Log extends Model
{
	public function admin()
  {
    //关联管理员表一对一
    return $this->belongsTo('Admin','admin_id','id');
  }

  public function menu()
  {
    //关联菜单表一对一
    return $this->belongsTo('auth_rule','rule_id','id');
  }

  public function getAll($post)
  {
    if (isset($post['rule_id']) and $post['rule_id'] > 0) {
      $where['rule_id'] = $post['rule_id'];
    }

    if (isset($post['admin_id']) and $post['admin_id'] > 0) {
        $where['admin_id'] = $post['admin_id'];
    }

    if(isset($post['create_time']) and !empty($post['create_time'])) {
      $min_time = strtotime($post['create_time']);
      $max_time = $min_time + 24 * 60 * 60;
      $where['create_time'] = [['>=',$min_time],['<=',$max_time]];
    }

    $log = empty($where) ? $this->order('create_time desc')->paginate(20) : $this->where($where)->order('create_time desc')->paginate(20,false,['query'=>$post]);
    return $log;
  }
}
