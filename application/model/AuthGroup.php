<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 权限组模型
// +----------------------------------------------------------------------
namespace app\model;

use think\Model;

class AuthGroup extends Model
{
  public static function getAllGroup($post)
  {
    if (isset($post['keywords']) and !empty($post['keywords'])) {
      $where['name'] = ['like', '%' . $post['keywords'] . '%'];
    }
    if(isset($post['create_time']) and !empty($post['create_time'])) {
      $min_time = strtotime($post['create_time']);
      $max_time = $min_time + 24 * 60 * 60;
      $where['create_time'] = [['>=',$min_time],['<=',$max_time]];
    }
    //这里有bug模糊查询结果为空
    $cater = empty($where)
      ? self::order('create_time desc')
        ->paginate(20)
      : self::where($where)
        ->order('create_time desc')
        ->paginate(20,false,['query'=>$post]);
    return $cater;
  }

  public function add($post)  
  {
    if(!empty($post['admin_menu_id'])) {
      $post['permissions'] = implode(',',$post['admin_menu_id']);
    }
    if(empty($post['status'])) {
      $post['status'] = 2;
    }
    $result = $this->allowField(true)->save($post);
    return $result;
  }

  public function edit($post,$id)
  {
    if(!empty($post['auth_rule_id'])) {
      $post['permissions'] = implode(',',$post['auth_rule_id']);
    } else {
      $post['permissions'] = '0';
    }
    if(empty($post['status'])) {
      $post['status'] = 2;
    }
    $result = $this->allowField(true)->save($post,['id'=>$id]);
    return $result;
  }
}
?>