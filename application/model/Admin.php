<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员模型
// +----------------------------------------------------------------------
namespace app\model;

class Admin extends BaseModel
{
  public function groupCates()
  {
    //关联角色表 一对一
    return $this->belongsTo('AuthGroup','group_id','id');
  }

  public static function getAdmin()
  {
    if (isset($post['keywords']) and !empty($post['keywords'])) {
      $where['name'] = ['like', '%' . $post['keywords'] . '%'];
    }
    if (isset($post['group_id_id']) and $post['group_id_id'] > 0) {
      $where['group_id_id'] = $post['group_id_id'];
    }
    if(isset($post['create_time']) and !empty($post['create_time'])) {
      $min_time = strtotime($post['create_time']);
      $max_time = $min_time + 24 * 60 * 60;
      $where['create_time'] = [['>=',$min_time],['<=',$max_time]];
    }
    $admin = empty($where) ? self::order('create_time desc')->paginate(20) : self::where($where)->order('create_time desc')->paginate(20,false,['query'=>$post]);
    return $admin;
  }

  public function add($post)
  {
    $post['password'] = password($post['password']);
    $result = $this->allowField(true)->save($post);
    return $result;
  }

  public function edit($post,$id)
  {
    $result = $this->allowField(true)->save($post,['id'=>$id]);
    return $result;
  }

  public static function login($app)
  {
    session('admin', $app->id);
    session('group_id', $app->group_id);
    addLog();
  }

  public static function check($ac, $se)
  {
    $app = self::where('app_id','=',$ac)->where('password','=',password($se))->find();
    return $app;
  }
}
?>