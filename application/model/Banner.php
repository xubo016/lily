<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 轮播图
// +----------------------------------------------------------------------
namespace app\model;

class Banner extends BaseModel
{
  protected $hidden = ['delete_time','update_time'];

  public function items()
  {
    //关联banner_item表 关系一对多
    return $this->hasMany('bannerItem','banner_id','id');
  }

  public static function getBannerByID($id)
  {
    $banner = self::with(['items','items.img'])->find($id);
    return $banner;
  }
  
}
?>