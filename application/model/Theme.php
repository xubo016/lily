<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 主题
// +----------------------------------------------------------------------
namespace app\model;

class Theme extends BaseModel
{
  protected $hidden = ['delete_time','update_time','topic_img_id','head_img_id'];

  public function topicImg(){
    // 关联attachment表 关系一对一
    return $this->belongsTo('attachment','topic_img_id','id');
  }

  public function headImg(){
    // 关联attachment表 关系一对一
    return $this->belongsTo('attachment','head_img_id','id');
  }

  public function products(){
    // 关联产品表 表关系多对多
    return $this->belongsToMany('product','theme_product','product_id','theme_id');
  }

  public static function getThemeWithProducts($id)
  {
    $theme = self::with('products,topicImg,headImg')->find($id);
    return $theme;
  }
}
?>