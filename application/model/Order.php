<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 订单
// +----------------------------------------------------------------------
namespace app\model;

class Order extends BaseModel
{
  protected $hidden = ['delete_time','update_time','user_id'];

  // 读取器
  public function getSnapItemsAttr($value)
  {
    if(empty($value)) {
      return null;
    }
    return json_decode($value);
  }

  public function getSnapAddressAttr($value)
  {
    if(empty($value)) {
      return null;
    }
    return json_decode($value);
  }

  // 分页查询历史订单
  public static function getSummaryByUser($uid, $page=1, $size=15)
  {
    $pagingData = self::where('user_id','=',$uid)
      ->order('create_time desc')
      ->paginate($size,true,['page'=>$page]);
    return $pagingData;
  }

  // 分页查询所有历史订单数据
  public static function getSummaryByPage($page=1, $size=20)
  {
    $pagingData = self::order('create_time desc')->paginate($size, true, ['page' => $page]);
    return $pagingData;
  }
}
?>