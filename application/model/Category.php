<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 商品
// +----------------------------------------------------------------------
namespace app\model;

class Category extends BaseModel
{
  protected $hidden = ['delete_time','update_time','create_time'];

  public function img()
  {
    // 关联图片表 表关系一对一
    return $this->belongsTo('attachment','topic_img_id','id');
  }
  
}
?>