<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 用户表
// +----------------------------------------------------------------------
namespace app\model;

class User extends BaseModel
{
  protected $hidden = ['delete_time','update_time','extend'];

  public function address()
  {
    // 关联地址表 关系一对一
    return $this->hasOne('userAddress','user_id','id');
  }

  // 查询用户是否存在
  public static function getByOpenID($openid)
  {
    $user = self::where('openid','=',$openid)->find();
    return $user;
  }

}
?>