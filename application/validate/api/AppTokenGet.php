<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员登录
// +----------------------------------------------------------------------
namespace app\validate\api;

class AppTokenGet extends ApiValidate
{
  protected $rule = [
    'ac' => 'require|isNotEmpty',
    'se' => 'require|isNotEmpty'
  ];
}
