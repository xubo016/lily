<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 历史订单简要信息接口
// +----------------------------------------------------------------------
namespace app\validate\api;

class PagingParameter extends ApiValidate
{
  protected $rule = [
    'page' => 'isPositiveInteger',
    'size' => 'isPositiveInteger'
  ];

  protected $message = [
    'page' => '分页参数必须是正整数',
    'size' => '分页参数必须是正整数'
  ];
}