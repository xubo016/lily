<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 下单接口验证
// +----------------------------------------------------------------------
namespace app\validate\api;

use app\lib\exception\ParameterException;
use think\Exception;

class OrderPlace extends ApiValidate
{
  protected $rule = [
      'products' => 'checkProducts'
  ];

  protected $singleRule = [
      'product_id' => 'require|isPositiveInteger',
      'count' => 'require|isPositiveInteger',
  ];

  protected function checkProducts($values)
  {
    if(!is_array($values)){
      throw new ParameterException([
        'msg' => '商品参数不正确'
      ]);
    }

    if(empty($values)){
      throw new ParameterException([
        'msg' => '商品列表不能为空'
      ]);
    }
    foreach ($values as $value)
    {
      $this->checkProduct($value);
    }
    return true;
  }

  private function checkProduct($value)
  {
    $validate = new ApiValidate($this->singleRule);
    $result = $validate->check($value);
    if(!$result){
      throw new ParameterException([
        'msg' => '商品列表参数错误',
      ]);
    }
  }
}