<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 
// +----------------------------------------------------------------------
namespace app\validate\api;

class IDCollection extends ApiValidate
{
  protected $rule =[
    'ids' => 'require|checkIDs'
  ];

  protected $message = [
    'ids' => 'ids参数必须为以逗号分隔的多个正整数'
  ];

  // ids = id1,id2....
  protected function checkIDs($value)
  {
    $values = explode(',',$value);
    if (empty($values)) return false;
    foreach ($values as $id) {
      if (!$this->isPositiveInteger($id)) {
        return false;
      }
    }
    return true;
  }

}

?>