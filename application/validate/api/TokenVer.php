<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 令牌有效期验证
// +----------------------------------------------------------------------
namespace app\validate\api;

class TokenVer extends ApiValidate
{
  protected $rule = [
    'token' => 'require|isNotEmpty'
  ];
  
  protected $message=[
    'token' => 'token不允许为空'
  ];
}
