<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 令牌验证
// +----------------------------------------------------------------------
namespace app\validate\api;

class TokenGet extends ApiValidate
{
  protected $rule = [
    'code' => 'require|isNotEmpty'
  ];
  
  protected $message=[
    'code' => '没有code还想拿token？做梦哦'
  ];
}
