<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class Password extends AdminValidate
{
  protected $rule = [
    'password' => 'require|confirm|min:6|max:12',
    'password_confirm' => 'require'
  ];

  protected $message = [
    'password.require' => '密码不能为空',
    'password_confirm' => '重复密码不能为空',
    'password.confirm' => '两次密码输入不一致',
    'password.min' => '密码位数有误',
    'password.max' => '密码位数有误',
  ];
}
?>