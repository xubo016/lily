<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 管理员验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class NickName extends AdminValidate
{
  protected $rule = [
    'app_id' => 'require|alpha|unique:admin',
  ];

  protected $message = [
    'app_id.require' => '昵称不能为空',
    'app_id.alpha' => '昵称格式只能是字母',
    'app_id.unique' => '该昵称已被注册',
  ];  
}
?>