<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 手机号验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class Phone extends AdminValidate
{
  protected $rule = [
    'phone' => 'require|isMobile|isNotEmpty',
  ];

  protected $message = [
    'phone.require'    => '手机号码不能为空',
    'phone.isMobile'   => '手机号码格式不正确',
    'phone.isNotEmpty' => '手机号码不能为空'
  ];
}
?>