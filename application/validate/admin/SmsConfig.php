<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 短信配置验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class SmsConfig extends AdminValidate
{
  protected $rule = [
    'appkey' => 'require',
    'secretkey' => 'require',
    'name' => 'require',
    'code' => 'require',
  ];

  protected $message = [
    'appkey.require' => 'AppKey不能为空',
    'secretkey.require' => 'SecretKey不能为空',
    'name.require' => '短信签名不能为空',
    'code.require' => '短信模板ID不能为空'
  ];
}
?>