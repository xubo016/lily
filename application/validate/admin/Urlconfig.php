<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain url美化验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class Urlconfig extends AdminValidate
{
  protected $rule = [
    'url' => 'require',
    'aliases' => 'require'
  ]; 

  protected $message = [
    'url.require' => '需要美化的url不能为空',
    'aliases.require' => '美化后的url不能为空'
  ];


}
?>