<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 权限组验证
// +----------------------------------------------------------------------
namespace app\validate\admin;

class AuthGroup extends AdminValidate
{
  protected $rule = [
    'name' => 'unique:auth_group|require',
  ];

  protected $message = [
    'name.require' => '提交失败：角色名不能为空',
    'name.unique' => '提交失败：角色名不能重复',
  ];

}
?>