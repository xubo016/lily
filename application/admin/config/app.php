<?php
return [
  //验证码
  'verify' => [
    // 验证码字符集合
    'codeSet'     => '0',
    // 验证码字体大小
    'fontSize'    => 25,
    // 验证码位数
    'length'      => 4,
    //是否开启中文验证码
    'useZh'       => false,
    //中文验证码字符集
    'zhSet'       => '',
    // 验证成功后是否重置
    'reset'       => true,
    //过期时间S
    'expire'      => 10000,
    // 是否画混淆曲线
    'useCurve'    => true,
    // 关闭验证码杂点
    'useNoise'    => false,
    'bg'          => [255,255,255],
  ],
];

?>