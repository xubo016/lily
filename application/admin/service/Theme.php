<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 
// +----------------------------------------------------------------------
namespace app\admin\service;

use app\admin\service\Attachment;
use think\Db;

class Theme
{
  public static function del($id)
  {
    $topicImgId = Db::name('theme')->where('id',$id)->value('topic_img_id');
    $headImgId = Db::name('theme')->where('id',$id)->value('head_img_id');
    $result1 = Attachment::delete($topicImgId); // 删除图片
    $result2 = Attachment::delete($headImgId); // 删除图片
    if ($result1 && $result2) {
      Db::startTrans();
      $theme = Db::name('theme')->where('id',$id)->delete();
      $pro = Db::name('theme_product')->where('theme_id','=',$id)->delete();
      Db::commit();
      if ($theme && $pro !== false) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
} 
?>