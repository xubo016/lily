<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 商品增删改
// +----------------------------------------------------------------------
namespace app\admin\service;

use app\model\ProductImage as ProductImageModel;
use app\model\Product as productModel;
use app\admin\service\Attachment;
use think\Db;

class Product
{
  // 添加商品
  public static function save($post)
  {
    $model = new productModel();
    Db::startTrans();
    $result = $model->save($post);
    // 添加商品主题
    if(!isset($post['theme_id'])){
      $post['theme_id'] = [];
    }
    $addTheme = self::saveProTheme($post['theme_id'],$model->id);
    // 添加商品图集
    if(!isset($post['pro_img_id'])){
      $post['pro_img_id'] = [];
    }
    if(isset($post['names']) && isset($post['detail'])) {
      self::property($post,$model->id);
    }
    $saveImg = self::saveProImg($post['pro_img_id'],$model->id);
    if ( $result && $addTheme && $saveImg) {
      Db::commit();
      return true;
    } else {
      Db::rollback();
      return false;
    }
  }

  // 添加商品详情
  private static function property($post, $productId)
  {
    foreach($post['names'] as $vo) {
      $list[] = ['name' => $vo, 'product_id' => $productId];
    }
    for($i = 0; $i < count($post['detail']); $i++) {
      $list[$i]['detail'] = $post['detail'][$i];
    }
    Db::name('product_property')->insertAll($list);
  }

  // 添加商品主题
  private static function saveProTheme($themeId,$productId)
  {
    if ($themeId) {
      foreach($themeId as $vo) {
        $list[] = ['theme_id' => $vo, 'product_id' => $productId];
      }
      $add = Db::name('theme_product')->insertAll($list);
      if ($add) {
        return true;
      }
      else {
        return false;
      }
    } 
    else {
      return true;
    }
  }

  // 添加商品图集
  private static function saveProImg($imgId,$productId)
  {
    $imgModel = new ProductImageModel();
    if ($imgId) {
      foreach ($imgId as $vo) {
        $list[] = ['img_id' => $vo, 'product_id' => $productId];
      }
      $save = $imgModel->saveAll($list);
      if ($save) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }
  }

  // 修改商品
  public static function edit($post,$id)
  {
    Db::startTrans();
    $model = new productModel();
    $result = $model->save($post,$id);
    // 修改商品主题
    if (!isset($post['theme_id'])) {
      $post['theme_id'] = [];
    }
    $editTheme = self::editTheme($post['theme_id'],$id);
    // 修改商品图集
    if(!isset($post['pro_img_id'])){
      $post['pro_img_id'] = [];
    }
    if(!isset($post['names']) && !isset($post['detail'])){
      $post['names'] = [];
      $post['detail'] = [];
    }
    self::editProperty($post,$id);
    $delImg = self::editProImages($post['pro_img_id'],$id);
    Db::commit();
    if ($result && $editTheme) {
      return true;
    } else {
      Db::rollback();
      return false;
    }
  }

  // 修改商品详情
  private static function editProperty($post, $productID)
  {
    Db::name('product_property')->where('product_id','=',$productID)->delete();
    self::property($post, $productID);
  }

  // 修改商品图集
  private static function editProImages($imgs,$productID)
  {
    $proImg = Db::name('product_image')->where('product_id','=',$productID)->column('img_id');
    if ($imgs) {
      foreach ($proImg as $vo) {
        if (!in_array($vo,$imgs)) {
          Attachment::delete($vo);
        }
      }
      Db::name('product_image')->where('product_id','=',$productID)->delete();
      foreach($imgs as $vo) {
        $list[] = ['img_id' => $vo, 'product_id' => $productID];
      }
      Db::name('product_image')->insertAll($list);
    }
    else {
      foreach ($proImg as $vo) {
        Attachment::delete($vo);
      }
      Db::name('product_image')->where('product_id','=',$productID)->delete();
    }
  }

  // 修改商品主题
  private static function editTheme($postThemeId,$productId)
  {
    Db::startTrans();
    $themeId = Db::name('theme_product')->where('product_id','=',$productId)->column('theme_id');
    $list = [];
    foreach ($postThemeId as $vo) {
      if (!in_array($vo,$themeId)) {
        $list[] = ['theme_id' => $vo, 'product_id' => $productId];
      }
    }
    if ($list) {
      $addTheme = Db::name('theme_product')->insertAll($list);
    }
    else {
      $addTheme = true;
    }
    $themes = [];
    foreach ($themeId as $vo) {
      if (!in_array($vo,$postThemeId)) {
        $themes[] = $vo;
      }
    }
    $delTheme = Db::name('theme_product')
      ->where('product_id','=',$productId)
      ->whereIn('theme_id',$themes)
      ->delete();
    Db::commit();
    if ($addTheme && $delTheme !==false) {
      return true;
    }
    else {
      Db::rollback();
      return false;
    }
  }

  // 商品删除
  public static function del($id)
  {
    $delProTheme = self::delProTheme($id);
    $delProImage = self::delProImage($id);
    $delProperty = self::delProperty($id);
    if ($delProTheme && $delProImage) {
      $product = Db::name('product')->where('id','=',$id)->delete();
      if ($product !==false) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  // 删除商品详情
  private static function delProperty($id)
  {
    Db::name('product_property')->where('product_id','=',$id)->delete();
  }

  // 删除商品主题
  private static function delProTheme($id)
  {
    $theme = Db::name('theme_product')->where('product_id','=',$id)->delete();
    if ($theme !== false) {
      return true;
    }
    else {
      return false;
    }
  }

  // 删除商品图集
  private static function delProImage($id)
  {
    $proImg = Db::name('product_image')->where('product_id','=',$id)->column('img_id');
    if ($proImg) {
      foreach ($proImg as $vo) {
        $result = Attachment::delete($vo); // 删除图片
        if (!$result) {
          return false;
        }
      }
      $pro_img = Db::name('product_image')->where('product_id','=',$id)->delete();
      if ($pro_img !== false) {
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return true;
    }
  }
} 
?>