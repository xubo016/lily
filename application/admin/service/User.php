<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 用户
// +----------------------------------------------------------------------
namespace app\admin\service;

use think\Db;

class User
{
  // 删除用户
  public static function delete($id)
  {
    Db::startTrans();
    $user = Db::name('user')->where('id',$id)->delete();
    $address = Db::name('user_address')->where('user_id','=',$id)->delete();
    Db::commit();
    if ($user && $address !== false) {
      return true;
    } 
    else {
      Db::rollback();
      return false;
    }
  }
} 
?>