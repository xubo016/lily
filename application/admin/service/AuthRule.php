<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 系统逻辑层
// +----------------------------------------------------------------------
namespace app\admin\service;

use think\facade\Session;
use think\Db;

class AuthRule
{
  public static function getAuth($menu)
  {
    $group_id = Session::get('group_id');
    $permissions = Db::name('auth_group')->where(['id'=>$group_id])->value('permissions');
    $permissions = explode(',',$permissions);
    foreach ($menu as $k => $val) {
      if($val['type'] == 1 and $val['is_display'] == 1 and !in_array($val['id'],$permissions)) {
        unset($menu[$k]);
      }
    }
    $menus = self::getUrl($menu);
    return $menus;
  }

  protected static function getUrl($menu)
  {
    foreach ($menu as $key => $value) {
      if(empty($value['parameter'])) {
          $url = url($value['module'].'/'.$value['controller'].'/'.$value['function']);
      } else {
          $url = url($value['module'].'/'.$value['controller'].'/'.$value['function'],$value['parameter']);
      }
      $menu[$key]['url'] = $url;
    }
    $menus = self::menuList($menu);
    return $menus;
  }

  protected static function menuList($menu)
  {
    $menu = $menu->toArray();
		$menus = array();
		//先找出顶级菜单
		foreach ($menu as $k => $val) {
			if($val['pid'] == 0) {
				$menus[$k] = $val;
      }
    }

		//通过顶级菜单找到下属的子菜单
		foreach ($menus as $k => $val) {
			foreach ($menu as $key => $value) {
				if($value['pid'] == $val['id']) {
					$menus[$k]['list'][] = $value;
				}
			}
    }
    
		//三级菜单
		foreach ($menus as $k => $val) {
			if(isset($val['list'])) {
				foreach ($val['list'] as $ks => $vals) {
					foreach ($menu as $key => $value) {
						if($value['pid'] == $vals['id']) {
							$menus[$k]['list'][$ks]['list'][] = $value;
            }
          }
				}
			}
		}
		return $menus;
  }

  public static function menuTree($menu,$id=0,$level=0){
    static $menus = array();
    $size = count($menus)-1;
		foreach ($menu as $value) {
			if ($value['pid']==$id) {
				$value['level'] = $level+1;
				$menus[] = $value;
				self::menuTree($menu,$value['id'],$value['level']);
			}
    }
		return $menus;
  }
  
  public static function ruleList($menu,$id=0,$level=0){
    static $menus = array();
    $size = count($menus)-1;
    foreach ($menu as $value) {
        if ($value['pid']==$id) {
            $value['level'] = $level+1;
            if($level == 0)
            {
              $menus[] = $value;
            }
            elseif($level == 2)
            {
              $menus[$size]['list'][] = $value;
            }
            elseif($level == 3)
            {
              $menus[$size]['list'][] = $value;
            }
            else
            {
              $menus[$size]['list'][] = $value;
            }
            self::ruleList($menu,$value['id'],$value['level']);
        }
    }
    return $menus;
  }
}
?>