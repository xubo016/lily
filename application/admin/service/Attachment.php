<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 附件删除
// +----------------------------------------------------------------------
namespace app\admin\service;

use think\facade\Env;
use think\Db;

class Attachment
{
  // 删除附件
  public static function delete($imgId)
  {
    $attachment = Db::name('attachment')->where('id',$imgId)->value('filepath');
    if(file_exists(Env::get('root_path').'public'.$attachment)) {
      if(unlink(Env::get('root_path').'public'.$attachment)) {
        if(false == Db::name('attachment')->where('id',$imgId)->delete()) {
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    } else {
      if(false == Db::name('attachment')->where('id',$imgId)->delete()) {
        return false;
      } else {
        return true;
      }
    }
  }
} 
?>