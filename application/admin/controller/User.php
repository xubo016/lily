<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 用户信息
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\User as UserModel;
use app\admin\service\User as UserService;

class User extends Base
{
  public function lst()
  {
    $model = new UserModel();
    $user = $model::with('address')->order('create_time desc')->paginate(20);
    $this->assign('user',$user);
    return $this->fetch();
  }

  public function del()
  {
    if($this->request->isAjax()) {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      $result = UserService::delete($id);
      if ($result) {
        return $this->success('删除成功','user/lst');
      } else {
        return $this->error('删除失败');
      }  
    }
  }


}
?>