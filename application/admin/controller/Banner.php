<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 轮播图管理
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\Banner as bannerModel;
use think\Db;

class Banner extends Base
{
  public function lst()
  {
    $banner = Db::name('banner')->paginate(20);
    $this->assign('banner',$banner);
    return $this->fetch();
  }

  public function addEdit()
  {
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new bannerModel();
    if($id > 0) {
      //是修改操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        if ($model->save($post,$id)) {
          return $this->success('修改信息成功','banner/lst');
        } else {
          return $this->error('修改失败');
        }
      } else {
        $info = Db::name('banner')->where('id',$id)->find();
        $this->assign('info',$info);
        return $this->fetch();
      }
    } else {
      //是新增操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        if($model->save($post)) {
          return $this->success('添加成功','banner/lst');
        } else {
          return $this->error('添加失败');
        }
      } else {
        return $this->fetch();
      }
    }
  }

  public function del()
  {
    if($this->request->isAjax()) {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      if(false == Db::name('banner')->where('id',$id)->delete()) {
        return $this->error('删除失败');
      } else {
        return $this->success('删除成功','banner/lst');
      }
    }
  }
}
?>