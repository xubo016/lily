<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 产品分类
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\Category as categoryModel;
use app\admin\service\Attachment;
use think\Db;

class Category extends Base
{
  public function lst()
  {
    $category = Db::name('category')->paginate(20);
    $this->assign('category',$category);
    return $this->fetch();
  }

  public function addEdit()
  {
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new categoryModel();
    if($id > 0) {
      //是修改操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        if ($model->save($post,$id)) {
          return $this->success('修改成功','category/lst');
        } else {
          return $this->error('修改失败');
        }
      } else {
        $info = Db::name('category')->where('id',$id)->find();
        $this->assign(['info' => $info]);
        return $this->fetch();
      }
    } else {
      //是新增操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        if($model->save($post)) {
          return $this->success('添加成功','category/lst');
        } else {
          return $this->error('添加失败');
        }
      } else {
        return $this->fetch();
      }
    }
  }

  public function del()
  {
    if($this->request->isAjax()) {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      $imgId = Db::name('category')->where('id',$id)->value('topic_img_id');
      $result = Attachment::delete($imgId); // 删除图片
      if($result) {
        if(false == Db::name('category')->where('id',$id)->delete()) {
          return $this->error('删除失败');
        } else {
          return $this->success('删除成功','category/lst');
        }
      } else {
        $this->error('删除失败');
      }
      
    }
  }


}
?>