<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 公共方法
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\Db;
use app\model\Attachment as AttachmentModel;
use think\facade\Cache;

class Common extends Base
{
  /**
   * 清除全部缓存
   * @return [type] [description]
   */
  public function clear()
  {
    if(false == Cache::clear()) {
      return $this->error('清除缓存失败');
    } else {
      return $this->success('清除缓存成功');
    }
  }

  /**
   * 图片上传方法 
   * @return [type] [description]
   */
  public function upload($module='admin',$use='admin_thumb')
  {
    if($this->request->file('file')){
      $file = $this->request->file('file');
    } else {
      $res['code']=1;
      $res['msg']='没有上传文件';
      return json($res);
    }
    $module = $this->request->has('module') ? $this->request->param('module') : $module;//模块
    $web_config = Db::name('web_config')->where('web','web')->find();
    $info = $file->validate(['size'=>$web_config['file_size']*1024,'ext'=>$web_config['file_type']])->rule('date')->move('uploads' . '/'. $module . '/'. $use);
    if($info) {
      //写入到附件表
      $model = new AttachmentModel();
      $result = $model->add($module,$use,$info);
      $res['id'] = $result;
      $res['src'] = '/'.'uploads' . '/' . $module . '/' . $use . '/' . $info->getSaveName();
      $res['code'] = 2;
      addLog($res['id']);//记录日志
      return json($res);
    } else {
      // 上传失败获取错误信息
      return $this->error('上传失败：'.$file->getError());
    }
  }
}

?>