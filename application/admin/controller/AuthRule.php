<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 系统模块
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\AuthRule as AuthRuleModel;
use app\validate\admin\AuthRule as AuthRuleValidate;
use app\admin\service\AuthRule as AuthRuleService;
use think\Db;

class AuthRule extends Base
{
  public function lst()
  {
    $menus = AuthRuleModel::menuList();
    $this->assign('menus',$menus);
    return $this->fetch();
  }

  public function addEdit()
  {
    //获取菜单id
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new AuthRuleModel();
    if($id > 0) {
      //是修改操作
      if(request()->isPost()) {
        (new AuthRuleValidate())->goCheck();
        if($model->edit(input('post.'),$id)){
          addLog($model->id);//写入日志
          return $this->success('修改菜单信息成功','admin/AuthRule/lst');
        } else {
          return $this->error('修改菜单失败');
        }
      } else {
        //非提交操作
        $menu = AuthRuleModel::where('id',$id)->find();
        $menus = AuthRuleModel::select();
        $menus_all = AuthRuleService::menuTree($menus);
        $this->assign('menus',$menus_all);
        if(!empty($menu)) {
          $this->assign('menu',$menu);
          return $this->fetch();
        } else {
          return $this->error('id不正确');
        }
      }
    } else {
      //是新增操作
      if(request()->isPost()) {
        (new AuthRuleValidate())->goCheck();
        if($model->add(input('post.'))){
          addLog($model->id);//写入日志
          return $this->success('添加菜单成功','admin/AuthRule/lst');
        } else {
          return $this->error('添加菜单失败');
        }
      } else {
        //非提交操作
        $pid = $this->request->has('pid') ? $this->request->param('pid', null, 'intval') : null;
        if(!empty($pid)) {
          $this->assign('pid',$pid);
        }
        $menu = AuthRuleModel::select();
        $menus = AuthRuleService::menuTree($menu);
        $this->assign('menus',$menus);
        return $this->fetch();
      }
    }
  }

  public function delete()
  {
    if(request()->isAjax()) {
      $id = request()->has('id') ? request()->param('id', 0, 'intval') : 0;
      if(AuthRuleModel::where('pid',$id)->select() -> isEmpty()) {
        if(AuthRuleModel::where('id',$id)->delete()) {
          addLog($id);//写入日志
          return $this->success('删除成功','admin/AuthRule/lst'); 
        } else {
          return $this->error('删除失败');
        }
      } else {
          return $this->error('该菜单下还有子菜单，不能删除');
      }
    }
  }

  public function sort()
  {
    if(request()->isPost()) {
      $post = $this->request->post();
      $model = new AuthRuleModel();
      $result = $model->sort($post);
      if ($result) {
        addLog();//写入日志
        return $this->success('成功更新'.$result.'个数据','admin/AuthRule/lst');
      } else {
        return $this->error('更新失败');
      }
    }
  }
}
?>