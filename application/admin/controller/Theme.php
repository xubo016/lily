<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 精选主题
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\service\Theme as themeService;
use app\model\Theme as themeModel;
use think\Db;

class Theme extends Base
{
  public function lst()
  {
    $theme = Db::name('theme')->paginate(20);
    $this->assign('theme',$theme);
    return $this->fetch();
  }

  public function addEdit()
  {
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new themeModel();
    if($id > 0) {
      //是修改操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        if ($model->save($post,$id)) {
          return $this->success('修改成功','Theme/lst');
        } else {
          return $this->error('修改失败');
        }
      } else {
        $info = Db::name('theme')->where('id',$id)->find();
        $this->assign(['info' => $info]);
        return $this->fetch();
      }
    } else {
      //是新增操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        if($model->save($post)) {
          return $this->success('添加成功','theme/lst');
        } else {
          return $this->error('添加失败');
        }
      } else {
        return $this->fetch();
      }
    }
  }

  public function del()
  {
    if($this->request->isAjax()) {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      $result = themeService::del($id);
      if ($result) {
        return $this->success('删除成功','theme/lst');
      } else {
        return $this->error('删除失败');
      }
    }
  }

}
?>