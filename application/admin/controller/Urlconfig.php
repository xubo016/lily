<?php
// +----------------------------------------------------------------------
// | astp [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 网站路由（路径美化）模块
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\Urlconfig as UrlconfigModel;
use app\validate\admin\Urlconfig as UrlconfigValidate;
use Think\Db;

class Urlconfig extends Base
{
  public function lst()
  {
    $model = new UrlconfigModel();
    $urlconfig = $model->order('create_time desc')->paginate(20);
    $this->assign('urlconfig',$urlconfig);
    return $this->fetch();
  }

  public function addEdit()
  {
    //获取规则id
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new UrlconfigModel();
    //是正常添加操作
    if($id > 0) {
      //是修改操作
      if($this->request->isPost()) {
        (new UrlconfigValidate())->goCheck();
        $post = $this->request->post();
        $result = $model->edit($post,$id);
        if(!$result) {
          return $this->error('修改失败');
        } else {
          addLog($id);//写入日志
          return $this->success('修改成功','urlconfig/lst');
        }
      } else {
        $urlconfig = $model->where('id',$id)->find();
        if(!empty($urlconfig)) {
          $this->assign('urlconfig',$urlconfig);
          return $this->fetch();
        } else {
          return $this->error('id不正确');
        }
      }
    } else {
      //是新增操作
      if($this->request->isPost()) {
        (new UrlconfigValidate())->goCheck();
        $post = $this->request->post();
        $result = $model->add($post);
        if(!$result) {
          return $this->error('添加失败');
        } else {
          addLog($model->id);//写入日志
          return $this->success('添加成功','urlconfig/lst');
        }
      } else {
        return $this->fetch();
      }
    }
  }

  public function delete()
  {
    if($this->request->isAjax()) 
    {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      if(false == Db::name('urlconfig')->where('id',$id)->delete()) {
        return $this->error('删除失败');
      } else {
        addLog($id);//写入日志
        return $this->success('删除成功','urlconfig/lst');
      }
    }
  }

  public function status()
  {
    //获取文件id
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    if($id > 0) {
      if($this->request->isPost()) {
        //是提交操作
        $post = $this->request->post();
        $status = $post['status'];
        if(false == Db::name('urlconfig')->where('id',$id)->update(['status'=>$status])) {
          return $this->error('设置失败');
        } else {
          addLog($id);//写入日志
          return $this->success('设置成功','urlconfig/lst');
        }
      }
    } else {
      return $this->error('id不正确');
    }
  }
}
?>