<?php
// +----------------------------------------------------------------------
// | Tpli [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 用户组模块
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\model\AuthGroup as AuthGroupModel;
use app\model\AuthRule as AuthRuleModel;
use app\admin\service\AuthRule as AuthRuleService;
use app\validate\admin\AuthGroup as AuthGroupValidate;
use think\Db;

class AuthGroup extends Base
{
  public function lst()
  {
      $post = $this->request->param();
      $Cater = AuthGroupModel::getAllGroup($post);
    	$this->assign('Cater',$Cater);
    	return $this->fetch();
  }

  public function addEdit()
  {
    //获取角色id
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new AuthGroupModel();
    $menuModel = new AuthRuleModel();
    if($id > 0) {
      //是修改操作
      if($this->request->isPost()) {
        (new AuthGroupValidate())->goCheck();
        $post = $this->request->post();
        $result = $model->edit($post,$id);
        if($result) {
          addLog($model->id);//写入日志
          return $this->success('修改角色信息成功','admin/AuthGroup/lst');
        } else {
          return $this->error('修改失败');
        }
      } else {
        $info['cates'] = $model->where('id',$id)->find();
        if (!empty($info['cates']['permissions'])) {
          //将菜单id字符串拆分成数组
          $info['cates']['permissions'] = explode(',',$info['cates']['permissions']);
        }
        $menus = Db::name('auth_rule')->select();
        $info['menu'] = AuthRuleService::ruleList($menus);
        $this->assign('info',$info);
        return $this->fetch();
      }
    } else {
      //是新增操作
      if($this->request->isPost()) {
        (new AuthGroupValidate())->goCheck();
        //是提交操作
        $post = $this->request->post();
        $result = $model->add($post);
        if ($result) {
          addLog($model->id);//写入日志
          return $this->success('添加角色成功','admin/AuthGroup/lst');
        } else {
          return $this->error('添加角色失败');
        }
      } else {
        $menus = Db::name('auth_rule')->select();
        $info['menu'] = AuthRuleService::ruleList($menus);
        $this->assign('info',$info);
        return $this->fetch();
      }
    }
  }

  public function delete()
  {
    if($this->request->isAjax()) {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      if($id > 0) {
        if($id == 1) {
          return $this->error('超级管理员角色不能删除');
        }
        if(false == Db::name('auth_group')->where('id',$id)->delete()) {
          return $this->error('删除失败');
        } else {
          addLog($id);//写入日志
          return $this->success('删除成功','admin/AuthGroup/lst');
        }
      } else {
        return $this->error('id不正确');
      }
    }
  }

  public function preview()
  {
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $model = new AuthGroupModel();
    $info['cates'] = $model->where('id',$id)->find();
    if(!empty($info['cates']['permissions'])) {
      //将菜单id字符串拆分成数组
      $info['cates']['permissions'] = explode(',',$info['cates']['permissions']);
    }
    $menus = Db::name('auth_rule')->select();
    $info['menu'] = AuthRuleService::ruleList($menus);
    $this->assign('info',$info);
    return $this->fetch();
  }

}

?>