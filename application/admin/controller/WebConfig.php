<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 网站设置模块
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\validate\admin\WebConfig as WebConfigValidate;
use app\model\WebConfig as WebConfigModel;

class WebConfig extends Base
{
  public function index()
  {
    $model = new WebConfigModel();
    $web_config = $model->where('web','web')->find();
    $this->assign('web_config',$web_config);
    return $this->fetch();
  }

  public function edit()
  {
    if($this->request->isPost()) {
      (new WebConfigValidate)->goCheck();
      $post = $this->request->post();
      $model = new WebConfigModel();
      $result = $model->edit($post);
      if($result) {
        addLog();
        return $this->success('提交成功','WebConfig/index');
      } else {
        return $this->error('提交失败');
      }
    }
  }
}
