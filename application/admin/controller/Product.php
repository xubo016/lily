<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 产品详情
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\service\Product as productService;
use app\model\ProductImage as ProductImageModel;
use app\model\Product as productModel;
use think\Db;

class Product extends Base
{
  public function lst()
  {
    $model = new productModel();
    $product = $model::with('categorys,themes')->order('id desc')->paginate(20);
    $this->assign('product',$product);
    return $this->fetch();
  }

  public function addEdit()
  {
    $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
    $imageModel = new ProductImageModel();
    $model = new productModel();
    if($id > 0) {
      //是修改操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        $result = productService::edit($post,$id);

        // return $this->error($result);

        if ( $result ) {
          return $this->success('修改成功','product/lst');
        } else {
          return $this->error('修改失败');
        }
      } else {
        $category = Db::name('category')->select();
        $theme = Db::name('theme')->select();
        $info = $model::with('themes')->where('id',$id)->find();
        $productImage = $imageModel::where('product_id','=',$id)->column('img_id');
        $property = Db::name('product_property')->where('product_id','=',$id)->field('name,detail')->select();
        $this->assign([
          'info' => $info,
          'theme' => $theme,
          'category' => $category,
          'productImage' => $productImage,
          'property' => $property,
        ]);
        return $this->fetch();
      }
    } else {
      //是新增操作
      if($this->request->isPost()) {
        $post = $this->request->post();
        $result = productService::save($post);
        if($result) {
          return $this->success('添加成功','product/lst');
        } else {
          return $this->error('添加失败');
        }
      } else {
        $property = Db::name('product_property')->where('product_id','=',$id)->field('name,detail')->select();
        $category = Db::name('category')->select();
        $theme = Db::name('theme')->select();
        $this->assign([
          'category' => $category,
          'property' => $property,
          'theme' => $theme,
        ]);
        return $this->fetch();
      }
    }
  }

  public function del()
  {
    if($this->request->isAjax()) {
      $id = $this->request->has('id') ? $this->request->param('id', 0, 'intval') : 0;
      $result = productService::del($id);
      if($result) {
        return $this->success('删除成功','product/lst');
      } else {
        return $this->error('删除失败');
      }
    }
  }

}
?>