<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author 荒年 < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 后台登录模块
// +----------------------------------------------------------------------
namespace app\admin\controller;

use think\captcha\Captcha;
use think\facade\Config;
use think\Controller;
use Session;

class Login extends Controller
{
  public function index()
  {
    if(Session::has('admin') == false) {
      return $this->fetch();
    } else {
      $this->redirect('admin/index/index');
    }
  }

  /**
   * 管理员退出，清除名字为admin的session
   * @return [type] [description]
   */
  public function logout()
  {
    Session::delete('admin');
    Session::delete('group_id');
    if(Session::has('admin') or Session::has('group_id')) {
      return $this->error('退出失败');
    } else {
      return $this->success('正在退出...','admin/login/index');
    }
  }

  //验证码
  public function verify(){
    $captcha = new Captcha(Config::get('app.verify'));
    return $captcha->entry();
  }
}

?>