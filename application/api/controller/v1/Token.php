<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain Token令牌
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use app\api\service\Token as TokenService;
use app\validate\api\AppTokenGet;
use app\validate\api\TokenGet;
use app\validate\api\TokenVer;
use app\api\service\UserToken;
use app\api\service\AppToken;

class Token
{
  /**
   * Token令牌接口
   * 通过code码获取Token令牌
   * @url token/user
   * @http POST
   * @code 用户code码
   * 
   */
  public function getToken($code = '')
  {
    (new TokenGet())->goCheck();
    $ut = new UserToken($code);
    $token = $ut->get();
    return [
      'token' => $token
    ];
  }

  /**
   * 第三方应用获取令牌
   * 通过账号密码获取token
   * @url token/app_token?
   * @http POST
   * @ac = :ac se = :secret
   * 
   */
  public function getAppToken($ac = '', $se = '')
  {
    (new AppTokenGet())->goCheck();
    $app = new AppToken();
    $token = $app->get($ac,$se);
    return [
      'token' => $token
    ];
  }

  /**
   * Token令牌验证接口
   * 接收客户端令牌验证有效性
   * @url Token/verify
   * @http POST
   * @token 客户端令牌
   * 
   */
  public function verifyToken($token='')
  {
    (new TokenVer())->goCheck();
    $valid = TokenService::verifyToken($token);
    return [
      'isValid' => $valid
    ];
  }
  
}
?>