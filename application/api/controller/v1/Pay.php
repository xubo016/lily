<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 支付
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use app\validate\api\IDMustBePositiveInt;
use app\api\controller\BaseController;
use app\api\service\Pay as payService;
use app\api\service\WxNotify;

class Pay extends BaseController
{
  protected $beforeActionList = [
    'checkExclusiveScope' => ['only' => 'getPreOrder']
  ];

  /**
   * 预支付接口
   * 获取微信支付所需参数
   * @url Pay/pre_order
   * @http POST
   * @id order的id
   * 
   */
  public function getPreOrder($id = '')
  {
    (new IDMustBePositiveInt())->goCheck();
    $pay = new payService($id);
    return $pay->pay();
  }

  /**
   * 支付回调接口
   * 获取微信支付结果
   * @url Pay/notify
   * @http POST
   * @
   * 
   */
  public function receiveNotify()
  {
    // 检测库存量
    // 更新支付状态
    // 减库存
    // 如果成功：返回成功处理信息 否则返回没有成功信息
    $notify = new WxNotify();
    $notify->Handle();
  }
  
}
?>