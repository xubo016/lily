<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain banner图接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use app\validate\api\IDMustBePositiveInt;
use app\model\Banner as BannerModel;
use app\lib\exception\BannerMissException;

class Banner
{
  /**
   * banner图接口
   * 获取指定id的banner信息
   * @url /banner/:id
   * @http GET
   * @id banner的id
   * 
   */
  public function getBanner($id)
  {
    (new IDMustBePositiveInt())->goCheck();
    $banner = BannerModel::getBannerByID($id);
    if (!$banner) {
      throw new BannerMissException();
    }
    return $banner;
  }
  
}
?>