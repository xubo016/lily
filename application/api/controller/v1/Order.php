<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 订单接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use app\api\service\Order as OrderService;
use app\api\service\Token as TokenService;
use app\validate\api\IDMustBePositiveInt;
use app\api\controller\BaseController;
use app\validate\api\PagingParameter;
use app\lib\exception\OrderException;
use app\model\Order as orderModel;
use app\validate\api\OrderPlace;

class Order extends BaseController
{
  // 用户在选择商品后，向API提交包含他选的商品信息
  // API接收到信息后，需要检测订单相关商品的库存信息
  // 有库存，把订单数据存入数据库中，下单成功了，返回客户端消息，告诉客户端可以支付了
  // 调用我们的支付接口，进行支付
  // 还需要再次进行库存检测
  // 服务器这边就可以就可以调用微信支付接口进行支付
  // 小程序根据服务器返回的结果拉起微信支付
  // 微信给我们返回一个支付结果（异步）
  // 成功，也需要进行库存量检测
  // 成功进行库存量的扣除

  protected $beforeActionList = [
    'checkExclusiveScope' => ['only' => 'placeOrder'],
    'checkPrimaryScope' => ['only' => 'getSummaryByUser, getDetail'],
  ];

  /**
   * 历史订单接口
   * 查询历史订单简要信息
   * @url order/by_user?
   * @http GET
   * @page分页页数 size每页数据条数
   * 
   */
  public function getSummaryByUser($page=1, $size =15)
  {
    (new PagingParameter())->goCheck();
    $uid = TokenService::getCurrentUid();
    $pagingOrders = orderModel::getSummaryByUser($uid, $page, $size);
    if($pagingOrders->isEmpty()) {
      return [
        'data' => [],
        'current_page' => $pagingOrders->getCurrentPage(), // 当前分页码
      ];
    }
    $data = $pagingOrders->hidden(['snap_items','snap_address','prepay_id'])->toArray();
    return [
      'data' => $data,
      'current_page' => $pagingOrders->getCurrentPage(), // 当前分页码
    ];
  }

  /**
   * 订单详情接口
   * 获取指定id的订单详情
   * @url order/by_user?
   * @http GET
   * @id order的id
   * 
   */
  public function getDetail($id)
  {
    (new IDMustBePositiveInt())->goCheck();
    $orderDetail = orderModel::get($id);
    if(!$orderDetail) {
      throw new OrderException();
    }
    return $orderDetail->hidden(['prepay_id']);
  }

  /**
   * 下单接口
   * 通过商品信息添加订单
   * @url /order
   * @http POST
   * @
   * 
   */
  public function placeOrder()
  {
    (new OrderPlace())->goCheck();
    $products = input('post.products/a');
    $uid = TokenService::getCurrentUid();
    $order = new OrderService();
    $status = $order->place($uid,$products);
    return $status;
  }

  /**
   * 获取全部订单简要信息（分页）
   * @param int $page
   * @param int $size
   * @return array
   * @url /paginate
   * 
   */
  public function getSummary($page=1, $size=20)
  {
    (new PagingParameter())->goCheck();
    $pagingOrders = orderModel::getSummaryByPage($page, $size);
    if($pagingOrders->isEmpty()) {
      return [
        'current_page' => $pagingOrders->currentPage(),
        'data' => []
      ];
    }
    $data = $pagingOrders->hidden(['snap_items','snap_address'])->toArray();
    return [
      'data' => $data,
      'current_page' => $pagingOrders->getCurrentPage()
    ];
  }

  /**
   * 发货接口
   * 通过商品信息添加订单
   * @url /delivery
   * @http PUT
   * @ID order表的id
   * 
   */
  public function delivery($id)
  {
    (new IDMustBePositiveInt())->goCheck();
    $order = new OrderService();
    $success = $order->delivery($id);
    if($success){
      return new SuccessMessage();
    }
  }
  
}
?>