<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 主题接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use app\validate\api\IDMustBePositiveInt;
use app\lib\exception\ThemeException;
use app\validate\api\IDCollection;
use app\model\Theme as ThemeModel;

class Theme
{
  /**
   * 精品主题简要信息接口
   * 获取指定ids的Theme信息
   * @url /theme?ids=id1,id2,id3,.....
   * @http GET
   * @ids theme的id
   * 
   */
  public function getSimpleList($ids='')
  {
    (new IDCollection())->goCheck();
    $ids = explode(',',$ids);
    $result = ThemeModel::with('topicImg,headImg')->select($ids);
    if ($result->isEmpty()) {
      throw new ThemeException();
    }
    return $result;
  }

  /**
   * 主题详情接口
   * 获取指定id的Theme信息
   * @url /theme/:id
   * @http GET
   * @id theme的id
   * 
   */
  public function getComplexOne($id)
  {
    (new IDMustBePositiveInt())->goCheck();
    $theme = ThemeModel::getThemeWithProducts($id);
    if (!$theme) {
      throw new ThemeException();
    }
    return $theme;
  }
  
}
?>