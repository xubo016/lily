<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 产品接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use app\validate\api\IDMustBePositiveInt;
use app\lib\exception\ProductException;
use app\model\Product as ProductModel;
use app\validate\api\Count;

class Product
{
  /**
   * 最近新品接口
   * 获取指定条数的最新产品信息
   * @url /recent?count=
   * @http GET
   * @count 产品条数
   * 
   */
  public function getRecent($count=15)
  {
    (new Count())->goCheck();
    $products = ProductModel::getMostRecent($count);
    if ($products->isEmpty()) {
      throw new ProductException();
    }
    $products = $products->hidden(['summary']);
    return $products;
  }

  /**
   * 分类商品接口
   * 获取分类下的产品信息
   * @url /by_category?id=
   * @http GET
   * @id 分类表id
   * 
   */
  public function getAllInCategory($id)
  {
    (new IDMustBePositiveInt())->goCheck();
    $products = ProductModel::getProductsByCategoryID($id);
    if ($products->isEmpty()) {
      throw new ProductException();
    }
    $products = $products->hidden(['summary']);
    return $products;
  }

  /**
   * 商品详情接口
   * 获取单条商品详情信息
   * @url Product/:id
   * @http GET
   * @id 商品表id
   * 
   */
  public function getOne($id)
  {
    (new IDMustBePositiveInt())->goCheck();
    $product = ProductModel::getProductDetail($id);
    if (!$product) {
      throw new ProductException();
    }
    return $product;
  }
  
}
?>