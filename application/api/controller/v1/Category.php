<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 分类接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use app\lib\exception\CategoryException;
use app\model\Category as CategoryModel;

class Category
{
  /**
   * 分类信息接口
   * 获取所有分类信息
   * @url /all
   * @http GET
   * 
   */
  public function getAllCategories()
  {
    $categories = CategoryModel::all([],'img');
    if ($categories->isEmpty()) {
      throw new CategoryException();
    }
    return $categories;
  }
  
}
?>