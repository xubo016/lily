<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 
// +----------------------------------------------------------------------
namespace app\api\controller;

use app\api\service\Token as TokenService;
use think\Controller;
use think\Hook;

class BaseController extends Controller
{
  // public function initialize()
  // {
  //   Hook::listen('appInit',$params);
  // }

  // 初级权限验证
  protected function checkPrimaryScope()
  {
    TokenService::needPrimaryScope();
  }
  
  // 只有用户才能访问的接口权限
  protected function checkExclusiveScope()
  {
    TokenService::needExclusiveScope();
  }
  
}
?>