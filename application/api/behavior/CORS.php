<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain API跨域
// +----------------------------------------------------------------------
namespace app\api\behavior;

use think\Response;

class CORS
{
  public static function appInit(&$params)
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
    header('Access-Control-Allow-Methods: POST,GET');
    if(request()->isOptions()){
        exit();
    }
  }
}