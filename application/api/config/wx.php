<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 微信相关配置
// +----------------------------------------------------------------------
return [
  
  // 小程序app_id
  'app_id' => 'wxbd5300bfbe6423c2',

  // 小程序app_secret
  'app_secret' => '5afb24e969ebc1ccfcba4017bce91d77',

  // 微信使用code换取用户openid及session_key的url地址
  'login_url' => "https://api.weixin.qq.com/sns/jscode2session?" .
      "appid=%s&secret=%s&js_code=%s&grant_type=authorization_code",

  // 微信获取access_token的url地址
  'access_token_url' => "https://api.weixin.qq.com/cgi-bin/token?" .
      "grant_type=client_credential&appid=%s&secret=%s",
  
  // 小程序配置
  'wxConfig' => [

    // 调试模式
    'debug'  => true,
    // 小程序app_id
    'app_id' => 'wxbd5300bfbe6423c2',
    // 小程序app_secret
    'app_secret' => '5afb24e969ebc1ccfcba4017bce91d77',

    /**
      * 微信支付
      */
    'payment' => [
      'merchant_id'        => '1553654491',  // 商户号
      'key'                => 'c049f2c0054889a6ee0a8c437c1aa996',
      'cert_path'          => 'apiclient_cert.pem', // XXX: 绝对路径！！！！
      'key_path'           => 'apiclient_key.pem',      // XXX: 绝对路径！！！！(前往微信公众平台上下载)
      // 'notify_url'         => '默认的订单回调地址',      // 你也可以在下单时单独设置来想覆盖它
      // 'device_info'     => '013467007045764',
      // 'sub_app_id'      => '',
      // 'sub_merchant_id' => '',
      // ...
    ],

    'guzzle' => [
      'timeout' => 3.0, // 超时时间（秒）
      'verify' => true
    ]
  ],

];

?>