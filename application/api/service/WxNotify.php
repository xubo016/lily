<?php
// +----------------------------------------------------------------------
// | [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 支付回调
// +----------------------------------------------------------------------
namespace app\api\service;

use app\api\service\Order as orderService;
use app\model\Order as orderModel;
use app\lib\enum\OrderStatusEnum;
use app\model\Product;
use think\facade\Log;
use think\Exception;
use think\Db;

class WxNotify
{
  public function Handle()
  {
    //接收微信返回的数据数据,返回的xml格式
    $xmlData = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : file_get_contents("php://input");
    //将xml格式转换为数组
    $data = $this->FromXml($xmlData);
    $arr = array();
    //为了防止假数据，验证签名是否和返回的一样。
    //记录一下，返回回来的签名，生成签名的时候，必须剔除sign字段。
    $sign = $data['sign'];
    unset($data['sign']);
    if($sign == $this->getSign($data))
    {
      $this->NotifyProcess($data);
    }
    else
    {
      Log::error($data['return_msg']);   
    }	
  }

  // 根据返回的订单号做业务逻辑
  private function NotifyProcess($data)
  {
    if($data['result_code'] == 'SUCCESS' && $data['return_code']=="SUCCESS") {
      $orderNo = $data['out_trade_no'];
      Db::startTrans();
      try {
        $order = orderModel::where('order_no','=',$orderNo)->lock(true)->find();
        // 检测支付状态为未支付
        if($order->status == 1) {
          $service = new orderService();
          $stockStatus = $service->checkOrderStock($order->id);  // 检测库存
          if($stockStatus['pass']) {
            $this->updateOrderStatus($order->id,true);
            $this->reduceStock($stockStatus);
          } 
          else {
            $this->updateOrderStatus($order->id,false);
          }
        }
        Db::commit();
        return true;
      }
      catch(Exception $ex) {
        Db::rollback();
        Log::error($ex);
        return false;
      }
    }
    else {
      return true;
    }
  }

  // 消减库存量
  private function reduceStock($stockStatus)
  {
    foreach($stockStatus['pStatusArray'] as $singlePStatus) {
      Product::where('id','=',$singlePStatus['id'])->setDec('stock',$singlePStatus['counts']);
    }
  }

  // 更新支付状态
  private function updateOrderStatus($orderID,$success)
  {
    $status = $success ? OrderStatusEnum::PAID : OrderStatusEnum::PAID_BUT_OUT_OF;
    orderModel::where('id','=',$orderID)->update(['status'=>$status]);
  }

  // 将xml格式转换为数组
  private function FromXml($xml)
  {
    if (empty($xml)) {
      # 如果没有数据，直接返回失败
      return false;
    }
    //将XML转为array
    //禁止引用外部xml实体
    libxml_disable_entity_loader(true);
    $data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    return $data;
  }

  // 生成签名
  public function getSign($params) 
  {
    ksort($params);   //将参数数组按照参数名ASCII码从小到大排序
    foreach ($params as $key => $item) {
      if (!empty($item)) {         //剔除参数值为空的参数
        $newArr[] = $key.'='.$item;     // 整合新的参数数组
      }
    }
    $stringA = implode("&", $newArr);         //使用 & 符号连接参数
    $stringSignTemp = $stringA."&key="."key";        // key是在商户平台API安全里自己设置的
    $stringSignTemp = MD5($stringSignTemp);       //将字符串进行MD5加密
    $sign = strtoupper($stringSignTemp);      //将所有字符转换为大写
    return $sign;
  }
}
?>