<?php
// +----------------------------------------------------------------------
// | astp [ WE ONLY DO WHAT IS NECESSARY ]
// +----------------------------------------------------------------------
// | Author < 931274989@qq.com >
// +----------------------------------------------------------------------
// | Explain 我的文章
// +----------------------------------------------------------------------
namespace app\wechat\controller;

use EasyWeChat\Foundation\Application;
use think\facade\Session;
use think\facade\Config;
use think\Controller;
use think\Db;
 
class UserInfo extends Controller
{

  public function index()
  {
    $options = Config::get('wewhat.wxConfig');
    $app = new Application($options);
    $oauth = $app->oauth;
    // 未登录
    if (empty(Session::get('wechat_user'))) {
      Session::set('target_url','UserInfo/index');
      return $oauth->redirect();
    }
    // 已经登录过
    $user = Session::get('wechat_user');
    $article = Db::name('article')->where('openid','=',$user['openid'])->select();
    $this->assign('article',$article);
    return view();
  }

}
?>