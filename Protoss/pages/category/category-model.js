
import { Base } from '../../utils/base.js';

class Category extends Base{

  constructor() {
    super();
  }

  // 获取所有分类数据
  getCategoryType(callback) {
    var param = {
      url: 'category/all',
      sCallback: function (data) {
        callback && callback(data);
      }
    }
    this.request(param);
  }

  // 对应分类所有商品数据
  getProductsByCategory(id, callback) {
    var param = {
      url: 'product/by_category?id=' + id,
      sCallback: function (data) {
        callback && callback(data);
      }
    }
    this.request(param);
  }

}

export { Category };