
import { Base } from '../../utils/base.js';

class Order extends Base{

  constructor() {
    super();
    this._storageKeyName = 'newOrder';
  }

  // 下订单
  doOrder(param, callback) {
    var that = this;
    var allParams = {
      url: 'order',
      type: 'POST',
      data: {products: param},
      sCallback: function(data) {
        that.execSetStorageSync(true);
        callback && callback(data);
      },
      eCallback: function() {

      } 
    };
    this.request(allParams);
  }

  /**
   * 拉起支付
   * params；
   * orderNumber - {int} 订单id
   * return；
   * callback - {obj} 回调方法 ，返回参数 可能值 0：商品缺货等原因导致不能支付；1：支付失败或支付取消；2：支付成功
   * 
   */
  execPay(orderNumber, callback) {
    var allParams = {
      url: 'pay/pre_order',
      type: 'POST',
      data: { id: orderNumber },
      sCallback: function(data) {
        var timeStamp = data.timeStamp;
        if (timeStamp) { // 可以支付
          wx.requestPayment({
            timeStamp: timeStamp.toString(),
            nonceStr: data.nonceStr,
            package: data.package,
            signType: data.signType,
            paySign: data.paySign,
            success: function() {
              callback && callback(2);
            },
            fall: function() {
              callback && callback(1);
            }
          })
        }
        else {
          callback && callback(0);
        }
      }
    }
    this.request(allParams);
  } 
  
  // 获得订单的具体内容
  getOrderInfoById(id, callback) {
    var that = this;
    var allParams = {
      url: 'order/' + id,
      sCallback: function(data) {
        callback && callback(data);
      },
      eCallback: function(){

      }
    }
    this.request(allParams);
  }

  // 获取所有订单
  getOrders(pageIndex, callback) {
    var allParams = {
      url: 'order/by_user',
      data: {page: pageIndex},
      sCallback: data => {
        callback && callback(data);
      }
    }
    this.request(allParams);
  }

  // 用户是否有新的订单
  hasNewOrder() {
    var flag = wx.getStorageSync(this._storageKeyName);
    return flag == true;
  }

  // 本地缓存 保存更新
  execSetStorageSync(data) {
    wx.setStorageSync(this._storageKeyName, data)
  };
};

export { Order }; 