$(function(){
    layui.use('form',function(){
      var form = layui.form,$ = layui.jquery;
      //监听提交
      form.on('submit(login)', function(data){
          $userName = $('#app_id');
          $pwd = $('#password');
          if(!$userName.val()) {
              layer.msg('请输入账号', {icon: 2, anim: 6, time: 1000});
              return;
          }
          if(!$pwd.val()) {
              layer.msg('请输入密码', {icon: 2, anim: 6, time: 1000});
              return;
          }
          var params = {
              url:'token/app',
              type:'post',
              data:{ac:$userName.val(),se:$pwd.val()},
              sCallback:function(res){
                  if (res) {
                      window.base.setLocalStorage('token',res.token);
                      layer.msg('登录成功', {icon: 1, time: 1000}, function(){
                        location.reload(0);
                      });
                  }
              },
              eCallback:function(e){
                  if(e.status == 401){
                    layer.msg('帐号或密码错误', {icon: 2, anim: 6, time: 1000});
                  }
              }
          };
          window.base.getData(params);
          return false;
      });
    });

    // $(document).on('keydown','.normal-input',function(e){
    //     var e = event || window.event || arguments.callee.caller.arguments[0];
    //     if (e && e.keyCode == 13) {
    //         $('#login').trigger('click');
    //     }
    // });

});

