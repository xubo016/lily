<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

// 轮播图
Route::get('api/:version/banner/:id','api/:version.Banner/getBanner');

// 主题
Route::get('api/:version/theme','api/:version.Theme/getSimpleList');
Route::get('api/:version/theme/:id','api/:version.Theme/getComplexOne');

// 产品
Route::group('api/:version/product', function(){
  Route::get('/recent','api/:version.Product/getRecent');
  Route::get('/by_category','api/:version.Product/getAllInCategory');
  Route::get('/:id','api/:version.Product/getOne')->pattern(['id' => '\d+']);
});

// 分类
Route::get('api/:version/category/all','api/:version.Category/getAllCategories');

// Token令牌
Route::group('api/:version/token', function(){
  Route::post('/user','api/:version.Token/getToken');
  Route::post('/verify','api/:version.Token/verifyToken');
  Route::post('/app','api/:version.Token/getAppToken');
});

// 用户地址
Route::post('api/:version/address','api/:version.Address/createOrUpdateAddress');
Route::get('api/:version/address','api/:version.Address/getUserAddress');

// 订单
Route::group('api/:version/order', function(){
  Route::post('','api/:version.Order/placeOrder');
  Route::get('/:id','api/:version.Order/getDetail')->pattern(['id' => '\d+']);
  Route::get('/by_user','api/:version.Order/getSummaryByUser');
  Route::get('/paginate','api/:version.Order/getSummary');
  Route::put('/delivery','api/:version.Order/delivery');
});

// 支付
Route::post('api/:version/pay/pre_order','api/:version.Pay/getPreOrder');
Route::post('api/:version/pay/notify','api/:version.Pay/receiveNotify');