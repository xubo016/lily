<?php 
return array (
  'api/:version.banner/getbanner' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/banner/<id>',
      1 => 
      array (
        'version' => 1,
        'id' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.theme/getsimplelist' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/theme',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.theme/getcomplexone' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/theme/<id>',
      1 => 
      array (
        'version' => 1,
        'id' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.product/getrecent' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/product/recent',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.product/getallincategory' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/product/by_category',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.product/getone' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/product/<id>',
      1 => 
      array (
        'version' => 1,
        'id' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.category/getallcategories' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/category/all',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.token/gettoken' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/token/user',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'post',
    ),
  ),
  'api/:version.token/verifytoken' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/token/verify',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'post',
    ),
  ),
  'api/:version.token/getapptoken' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/token/app',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'post',
    ),
  ),
  'api/:version.address/createorupdateaddress' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/address',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'post',
    ),
  ),
  'api/:version.address/getuseraddress' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/address',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.order/placeorder' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/order',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'post',
    ),
  ),
  'api/:version.order/getdetail' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/order/<id>',
      1 => 
      array (
        'version' => 1,
        'id' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.order/getsummarybyuser' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/order/by_user',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.order/getsummary' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/order/paginate',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.order/delivery' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/order/delivery',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'get',
    ),
  ),
  'api/:version.pay/getpreorder' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/pay/pre_order',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'post',
    ),
  ),
  'api/:version.pay/receivenotify' => 
  array (
    0 => 
    array (
      0 => 'api/<version>/pay/notify',
      1 => 
      array (
        'version' => 1,
      ),
      2 => '',
      3 => NULL,
      4 => 'post',
    ),
  ),
);