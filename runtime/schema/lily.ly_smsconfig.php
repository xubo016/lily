<?php 
return array (
  'sms' => 
  array (
    'name' => 'sms',
    'type' => 'varchar(10)',
    'notnull' => false,
    'default' => 'sms',
    'primary' => false,
    'autoinc' => false,
  ),
  'appkey' => 
  array (
    'name' => 'appkey',
    'type' => 'varchar(200)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'secretkey' => 
  array (
    'name' => 'secretkey',
    'type' => 'varchar(200)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'type' => 
  array (
    'name' => 'type',
    'type' => 'varchar(100)',
    'notnull' => false,
    'default' => 'normal',
    'primary' => false,
    'autoinc' => false,
  ),
  'name' => 
  array (
    'name' => 'name',
    'type' => 'varchar(100)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'code' => 
  array (
    'name' => 'code',
    'type' => 'varchar(100)',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
  'content' => 
  array (
    'name' => 'content',
    'type' => 'text',
    'notnull' => false,
    'default' => NULL,
    'primary' => false,
    'autoinc' => false,
  ),
);